﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatCard : MonoBehaviour
{
    // For Delete
    int catid;

    // Tab Control // 0 = abilites, 1 = genetics, 2 = Other
    int tab = 0;
    SlotMaker slot;
    public GameObject scrollContainer;


    // Basic
    public CatInstance catModel;
    public Text catname;
    public Text nature;
    public Image gender;
    public Image element;


    // Abilities

    // Genetics

    // Other

    private void Start()
    {
        slot = this.GetComponent<SlotMaker>();
    }

    public void Reload()
    {
        CatData data = GameController_Script.control.getSelectedCatData();
        catModel.setCatData(data);
        catname.text = data.Name;
        nature.text = "Nature: TBD";

        //LoadGeneticsOfCat(data);

        //data.Gender == 0 ? "F" : "M"); //condition ? consequence : alternative
        //if (data.Gender == 1)
        //{

        //}
        //else
        //{

        //}

        //switch (expression)
        //{
        //    case x:
        //        // code block
        //        break;
        //    case y:
        //        // code block
        //        break;
        //    default:
        //        // code block
        //        break;
        //}


    }

    /// <summary>
    /// unused
    /// </summary>
    /// <param name="t"></param>
    public void ChangeTab(int t)
    {
        tab = t;
    }

    /// <summary>
    /// Fill the content pane with children
    /// </summary>
    /// <param name="t"></param>
    public void loadTab()
    {
        // Clear Previous
        if (transform.childCount > 0)
        {
            // Clear old
            foreach (Transform child in scrollContainer.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        // Populate based on which tab is selected
        switch (tab)
        {
            case 0:
                // code block
                break;
            case 1:
                // code block
                break;
            case 2:
                // code block
                break;
            default:
                // code block
                break;
        }
    }

    private void reloadAbilities()
    {


    }

   public void LoadGeneticsOfCat(CatData data)
    {

        if (data != null)
        {
            // clear does not work
            foreach (Transform child in scrollContainer.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            // Size
            //slot.AddCattribute("Size",CalculateCatSize(data.Size*data.CalculateSizePercent()) + "");

            // ---- COLORS ----
            // base
            slot.AddGene("Base Color",data.GetBaseColor());
            // eyes
            slot.AddGene("Eye Color",data.GetEyeColor());

            // ---- MARKINGS ----
            string[] marks = data.GetMarkingKeys();
            float[,] colors = data.GetMarkingColors();

            for (int i = 0; i < marks.Length; i++)
            {
                Color c = new Color(colors[i, 0], colors[i, 1], colors[i, 2]);
                slot.AddGene(marks[i] + "", c);
            }

            // get all traits
            string[] traits = data.GetTraitKeys();
            float[,] traitColors = data.GetTraitColors();

            for (int i = 0; i < traits.Length; i++)
            {
                Color c = new Color(traitColors[i, 0], traitColors[i, 1], traitColors[i, 2]);
                slot.AddGene(traits[i] + "",c);
            }
        }
    }

    private void reloadOther()
    {


    }

}
