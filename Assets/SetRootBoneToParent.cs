﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRootBoneToParent : MonoBehaviour
{
    public SkinnedMeshRenderer parent;
    // Start is called before the first frame update
    void Start()
    {
        Transform root = parent.rootBone;
        GetComponent<SkinnedMeshRenderer>().rootBone = root;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
