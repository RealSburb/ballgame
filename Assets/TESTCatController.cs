﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles collisions and Controlling the cat via taps
/// </summary>
public class TESTCatController : MonoBehaviour
{
    public enum Mode { Wander, Control, Interact, Joystick, Follow, Stationary};

    // Attributes
    public float movementSpeed;
    public float rotationSpeed;
    public Transform headBone;
    
    // Movement
    private Quaternion targetRot;

    // Utility
    public Mode mode = Mode.Wander;
    private WanderUtility wanderUtil;
    private Animator anim;
    ParticleSystem particles;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        wanderUtil = GetComponent<WanderUtility>();
        particles = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(mode == Mode.Wander)
        {
            anim.SetFloat("Speed", movementSpeed); //TODO find percent of maximum the joystick is at and multiply that by speed
        }
        else
        {
            anim.SetFloat("Speed", 0);
        }
    }

    /// <summary>
    /// Move to the position specified by playerRot. TODO Currently Click but will also handle target
    /// </summary>
    private void MoveForwards()
    {
        Vector3 m = Vector3.forward * (movementSpeed);
        transform.Translate(m);
    }

    /// <summary>
    /// Rotate to the target Rotation
    /// </summary>
    private void MoveRotation()
    {
        if (!transform.rotation.Equals(targetRot))
        {
            anim.SetFloat("Speed", movementSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * rotationSpeed);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void FixedUpdate()
    {
        if (mode == Mode.Wander)
        {
            Wander();
        }
        if (mode == Mode.Interact)
        {
            MoveRotation();
        }
    }

    private void Wander()
    {
        MoveForwards();
        targetRot = wanderUtil.GetWanderAndCollisionRot();
        MoveRotation();
    }

    /// <summary>
    /// Stop cat wandering
    /// </summary>
    public void StopWander()
    {
        mode = Mode.Stationary;
    }

    /// <summary>
    /// Start cat wandering
    /// </summary>
    public void StartWander()
    {
        mode = Mode.Wander;
    }

    /// <summary>
    /// Turn to face the camera
    /// </summary>
    public void FacePlayer()
    {
        StopWander();

        mode = Mode.Interact;
        Transform target = Camera.main.transform;
        Vector3 targetPoint = new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(targetPoint, Vector3.up);
        targetRot = targetRotation;
    }

    private void OnMouseDown()
    {
        particles.Play();
    }
    private void OnMouseDrag()
    {
        FacePlayer();
    }
    private void OnMouseUp()
    {
        particles.Stop();
    }

    ///// <summary>
    ///// Turn head bone towards camera
    ///// </summary>
    //private void TurnHeadToMe()
    //{
    //    //Todo Test
    //    Vector3 target = Camera.main.transform.forward;
    //    Vector3 origin = headBone.position; // get origin point of rotation
    //    Vector3 targetXZ = new Vector3(target.x, origin.y, target.z); // "neutralize" target's position so that it has the same y as origin
    //    headBone.LookAt(targetXZ);
    //}
}
