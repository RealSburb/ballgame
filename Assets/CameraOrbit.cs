﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Heavily based on https://www.youtube.com/watch?v=z7eojB_1wKg
/// </summary>
public class CameraOrbit : MonoBehaviour
{
    public Transform target;

    public float horizMove = 0.0001f; // angle
    public float vertMove = 15f;

    private void Start()
    {
        StartCoroutine(Rotate());
    }

    private IEnumerator Rotate()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            MoveHorizontal(true);
        }
    }

    public void MoveHorizontal(bool left)
    {
        float dir = left == true ? 1 : -1;
        transform.RotateAround(target.position,Vector3.up,horizMove * dir);
        transform.LookAt(target);
    }


    public void MoveVertical(bool up)
    {
        float dir = up == true ? 1 : -1;
        transform.RotateAround(target.position, transform.TransformDirection(Vector3.right), vertMove * dir); // local x axis
    }


}
