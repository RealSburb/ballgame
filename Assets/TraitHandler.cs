﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TraitHandler : MonoBehaviour
{
    public Trait[] possibleTraits;
    private Dictionary<string, Transform> dict = new Dictionary<string, Transform>();

    /// <summary>
    /// Returns a list of strings to game controller used by the cat maker to pick traits
    /// </summary>
    /// <returns></returns>
    public string[] GetTraitKeys()
    {
        string[] keys = new String[possibleTraits.Length];
        int i = 0;
        foreach (Trait t in possibleTraits)
        {
            keys[i] = t.name;
            i++;
        }

        /*string print = "";
        foreach (string s in keys)
        {
            print += s + " ";
        }
        Debug.Log("Trait Keys: " + print);*/
        return keys;
    }

    void Awake()
    {
        //setup trait Dictionary
        foreach (Trait t in possibleTraits)
        {
            dict.Add(t.name, t.mesh);
            //Debug.Log("Added " + t.name);
        }
    }

    public void TurnOnTraits(CatData data)
    {
        // turn on traits marked
        string[] traitkeys = data.GetTraitKeys();
        float[,] colors = data.GetTraitColors();
        int i = 0;
        foreach (string key in traitkeys)
        {
            // Get Trait and activate
            GameObject trait = dict[key].gameObject;
            trait.gameObject.SetActive(true);
            // Set Color
            Color newCol = new Color(colors[i,0], colors[i, 1], colors[i, 2]);
            SkinnedMeshRenderer rend = trait.GetComponent<SkinnedMeshRenderer>();
            // Get default materials from renderer
            Material[] defMaterials = rend.materials;
            // Set new color
            defMaterials[0].color = newCol;
            // Set the Trait's renderer to the new material
            rend.materials = defMaterials;
            i++;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

}

[Serializable]
public class Trait
{
    public string name;
    public Transform mesh;
}