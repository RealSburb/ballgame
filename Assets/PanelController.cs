﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public static PanelController controller;

    public CatCard catCardPanel;
    public GameObject profileObjs;

    /// <summary>
    /// Singleton instance, since there should only ever be one of these panels at a time
    /// </summary>
    void Awake()
    {
        //Debug.Log("InfoPanel Awake");
        // create Singleton
        if (controller == null)
        {
            controller = this;
            //Debug.Log("InfoPanel set");
        }
        else if (controller != this)
        {
            Destroy(gameObject);
        }
    }

    //public void OpenCatInfoPanel()
    //{

    //}


    public void OpenCatCard()
    {
        profileObjs.SetActive(true);
        catCardPanel.gameObject.SetActive(true);
        catCardPanel.Reload();
    }
    /// <summary>
    /// unused
    /// </summary>
    public void CloseCatCard()
    {
        profileObjs.SetActive(false);
        catCardPanel.gameObject.SetActive(false);
    }
}
