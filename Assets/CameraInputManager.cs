﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInputManager : MonoBehaviour
{
    CameraOrbit cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<CameraOrbit>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            cam.MoveHorizontal(true);
        }
    }
}
