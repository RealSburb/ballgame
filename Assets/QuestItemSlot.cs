﻿using UnityEngine;
using UnityEngine.UI;

public class QuestItemSlot : MonoBehaviour
{
    public Image img;
    public Text textObj;
    public Text textBonus;

    public void SetSprite(Sprite s)
    {
        img.sprite = s;
    }

    public void setText(int amt)
    {
        textBonus.text = amt * 2 + "";
        textObj.text = (0 + "/" + amt);
    }

    public void UpdateText(Quest q)
    {
        textObj.text = q.amtDone + "/" + q.amtNeeded;
    }
}
