﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableChildPrefab : MonoBehaviour
{
    // in CatMaker create a cat instance and then grab all of the children and pick from the array of traits
    public CatData data;
    public List<Trait> traits;
    private Dictionary<string,Transform> dict = new Dictionary<string, Transform>();

    // Start is called before the first frame update
    void Start()
    {
        SkinnedMeshRenderer[] rend = GetComponentsInChildren<SkinnedMeshRenderer>();
        //rend[0].name;

        //setup trait Dictionary
        foreach (Trait t in traits)
        {
            dict.Add(t.name, t.mesh);
        }

        // turn on traits marked
        string[] traitkeys = data.GetTraitKeys();
        foreach(string key in traitkeys)
        {
            dict[key].gameObject.SetActive(true);   
        }
    }

    // Update is called once per frame
    void Update()
    { 
    }
}