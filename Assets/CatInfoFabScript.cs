﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatInfoFabScript : MonoBehaviour
{
    public Text header;
    public Text descriptor;
    public Outline outlineScript;
    public Image backgroundImg;

    private void Start()
    {
        
    }

    /// <summary>
    /// Set up CatInfoPrefab as a Genetics Slot
    /// </summary>
    /// <param name="name"></param>
    /// <param name="color"></param>
    public void SetGene(string geneName, Color color)
    {
        backgroundImg.color = color;
        header.text = geneName + ":";
        descriptor.text = ColorUtility.ToHtmlStringRGB(color);
    }

    /// <summary>
    /// Set up a CatInfoPrefab as a Cattributes Slot
    /// </summary>
    /// <param name="name"></param>
    /// <param name="attribute"></param>
    public void SetCattributes(string title, string attribute)
    {
        backgroundImg.enabled = false;
        outlineScript.enabled = false;
        header.text = title + ":";
        descriptor.text = attribute;
    }


}
