﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderUtility : MonoBehaviour
{
 // Wander
    private float wanderTime;
    private Quaternion targetRotation;

    /// <summary>
    /// Return the rotation to rotate to
    /// </summary>
    /// <returns></returns>
    public Quaternion GetWanderAndCollisionRot()
    {
        // Alter path if there is an impending collision
        CheckForImpendingCollisions();
        // The coroutine is taking care of whether a direction change happens
        return targetRotation;
    }

    /// <summary>
    /// On Start start choosing a location to wander to every "wanderTime"
    /// </summary>
    private void Start()
    {
        StartCoroutine(ChangeWanderTarget());
    }

    /// <summary>
    /// Check for Impending Collisions in front of object ignoring the ground and Monster Tags
    /// </summary>
    private void CheckForImpendingCollisions()
    {
        float rayMagnitude = 10;
        Vector3 moveTo = transform.position + (transform.forward*rayMagnitude);

        Ray ray = new Ray(transform.position,moveTo);
        RaycastHit hit; // where the hit is stored

        if (Physics.Raycast(ray.origin, ray.direction, out hit, rayMagnitude) && !hit.transform.gameObject.CompareTag("Ground") && !hit.transform.gameObject.CompareTag("Prey") && !hit.transform.gameObject.CompareTag("Monster"))
        {
                // Move away
                Vector3 pos = transform.position;
                pos = new Vector3(-pos.x, 0, -pos.z);
                targetRotation = Quaternion.LookRotation(pos);
                // speed of the turn should be dependant on how close the collision is
        }
    }

    // ------------------ WANDER ----------------------- //
    /// <summary>
    /// Change direction every 5 seconds to 15 seconds
    /// </summary>
    /// <returns></returns>
    IEnumerator ChangeWanderTarget()
    {
        float wanderTime = Random.Range(5.0f, 15.0f);
        ChooseRotation();
        yield return new WaitForSeconds(wanderTime);
        StartCoroutine(ChangeWanderTarget());
    }

    /// <summary>
    /// Chose a new target to wander towards
    /// </summary>
    private void ChooseRotation()
    {
        // find a position at a random degrees away
        float angle = Random.Range(0, 360) - 180; // rotate from -180 to 180 across y
        // get current rotation
        Quaternion newRotation = transform.rotation;
        // multiply by the degree turn
        newRotation *= Quaternion.Euler(0, angle, 0);
        targetRotation = newRotation;
        //Debug.Log("NewRot: " + angle);
    }
}
