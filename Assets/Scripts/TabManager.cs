﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// For each genetic element add a text element and a color element to the array
/// </summary>
public class TabManager : MonoBehaviour //TabFiller
{
    //private MenuElementMaker ele;
    public SlotMaker slot;

    /// <summary>
    /// For using Unity inspector
    /// </summary>
    /// <param name="s"></param>
    public void loadTab(String s)
    {
        foreach(Tab t in Enum.GetValues(typeof(Tab)))
        {
            if (t.ToString().Equals(s))
            {
                loadTab(t);
            }
        }
    }


    /// <summary>
    /// Fill the content pane with children
    /// </summary>
    /// <param name="t"></param>
    public void loadTab(Tab t)
    {
        if(transform.childCount > 0)
        {
            // Clear old
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        // Populate based on which tab is selected
        if (t == Tab.Genetics)
        {
            LoadGeneticsOfSelectedCat();
        }
        if (t == Tab.Cattributes)
        {
            reloadCattributes();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void reloadCattributes()
    {
        CatData data = GameController_Script.control.getSelectedCatData();
        slot.AddCattribute("Sex", data.Gender == 0 ? "F" : "M"); //condition ? consequence : alternative

        slot.AddCattribute("Age",AssignAgeText());
        slot.AddCattribute("Level", data.GetLevel()+"");
        slot.AddCattribute("Kills",data.GetPowerups() + "");
        slot.AddCattribute("Role",getRoleText(data));

        slot.AddDivider();

        slot.AddCattribute("Strength",data.GetBaseHp() + "");
        slot.AddCattribute("Stealth",data.GetBaseStr() + "");
        slot.AddCattribute("Nature",data.GetBaseAcc() + "");
        slot.AddCattribute("Spirit",data.GetBaseStl() + "");

    }

    private void LoadGeneticsOfSelectedCat()
    {
        CatData data = GameController_Script.control.getSelectedCatData();
        LoadGeneticsOfCat(data);
    }

    public void LoadGeneticsOfCat(CatData data)
    {

        if (data != null)
        {
            // clear does not work
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            // Size
            slot.AddCattribute("Size",CalculateCatSize(data.Size*data.CalculateSizePercent()) + "");
            //TODO remove - This is for debuggings
            slot.AddCattribute("SizeMod",Math.Round(data.Size, 2) + "");

            // ---- COLORS ----
            // base
            slot.AddGene("Base Color",data.GetBaseColor());
            // eyes
            slot.AddGene("Eye Color",data.GetEyeColor());

            // ---- MARKINGS ----
            string[] marks = data.GetMarkingKeys();
            float[,] colors = data.GetMarkingColors();

            for (int i = 0; i < marks.Length; i++)
            {
                Color c = new Color(colors[i, 0], colors[i, 1], colors[i, 2]);
                slot.AddGene(marks[i] + "", c);
            }

            // get all traits
            string[] traits = data.GetTraitKeys();
            float[,] traitColors = data.GetTraitColors();

            for (int i = 0; i < traits.Length; i++)
            {
                Color c = new Color(traitColors[i, 0], traitColors[i, 1], traitColors[i, 2]);
                slot.AddGene(traits[i] + "",c);
            }
        }
    }

    /// <summary>
    /// Returns cat size in cm
    /// </summary>
    /// <param name="sizeMod"></param>
    /// <returns></returns>
    private string CalculateCatSize(float sizeMod)
    {
        float sizeCm = (sizeMod *10) + 15; // 25cm is an average cat biggest cat is 2*10 + 15 = 35cm, and smallest is 0.1 = 2*1 +15 = 17
        if(sizeMod < 100)
        {
            return Math.Round(sizeCm, 2) + "cm";
        }
        else
        {
            return Math.Round((sizeMod/100),2) + "m";
        }
    }

    public string AssignAgeText()
    {
        int age = GameController_Script.control.getSelectedCatData().GetLevel();

        if (age == 0)
        {
            return "Newborn";
        }
        else if (age == 1)
        {
            return "Kitten";
        }
        else if (age == 2)
        {
            return "Youngster";
        }
        else if (age == 3)
        {
            return "Teen";
        }
        else if (age <= 5)
        {
            return "Young Adult";
        }
        else if (age <= 10)
        {
            return "Adult";
        }
        else if (age <= 11) //TODO last two are unused
        {
            return "Elder";
        }
        else
        {
            return "Timeless";
        }
    }

    public String getRoleText(CatData data)
    {
        int highestID = 0;
        int highestValue = -1;
        int[] stats = data.GetBaseStats();
        for(int i = 0; i < stats.Length; i++)
        {
            if(stats[i] > highestValue)
            {
                highestID = i;
                highestValue = stats[i];
            }
        }

        switch (highestID)
        {
            case 0:
                return "Fighter";
            case 1:
                return "Hunter";
            case 2:
                return "Gatherer";
            case 3:
                return "Shaman";
            default:
                return "Ghost";
        }
    }

}
