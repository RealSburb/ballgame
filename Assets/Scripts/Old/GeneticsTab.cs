﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// For each genetic element add a text element and a color element to the array
/// </summary>
public class GeneticsTab : MonoBehaviour
{
    public Font arial;

    private GameObject createAndAddTextObj(string text)
    {
        GameObject textHolder = new GameObject();
        textHolder.AddComponent<Text>();
        Text textComp = textHolder.GetComponent<Text>();
        //textHolder.transform.SetParent(transform);
        //textHolder.transform.SetParent(this.transform, false); //false makes size not reset
        textComp.text = text;
        textComp.font = arial;
        textComp.color = Color.black;
        textComp.fontSize = 12;

        textHolder.transform.SetParent(transform, false);
        return textHolder;
    }

    public void reload()
    {
        CatData data = GameController_Script.control.getSelectedCatData();
        if(data != null)
        {
            // clear does not work
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            // base
            createAndAddTextObj("Base Color:");
            createAndAddTextObj(ColorUtility.ToHtmlStringRGB(data.GetBaseColor()));
            // eyes
            createAndAddTextObj("Eye Color:");
            createAndAddTextObj(ColorUtility.ToHtmlStringRGB(data.GetEyeColor()));
            // get all markings
            string[] marks = data.GetMarkingKeys();
            float[,] colors = data.GetMarkingColors();

            for(int i = 0; i < marks.Length; i++)
            {
                createAndAddTextObj(marks[i]+":");
                Color c = new Color(colors[i, 0], colors[i,1], colors[i,2]);
                createAndAddTextObj(ColorUtility.ToHtmlStringRGB(c));
            }

            // get all traits
            string[] traits = data.GetTraitKeys();
            float[,] traitColors = data.GetTraitColors();

            for (int i = 0; i < traits.Length; i++)
            {
                createAndAddTextObj(traits[i] + ":");
                Color c = new Color(traitColors[i, 0], traitColors[i, 1], traitColors[i, 2]);
                createAndAddTextObj(ColorUtility.ToHtmlStringRGB(c));
            }

        }
    }
}
