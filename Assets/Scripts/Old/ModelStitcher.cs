﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelStitcher : MonoBehaviour
{
    public GameObject mainObj;
    public GameObject addObj;

    private void Start()
    {
        StitchObj(mainObj, addObj);
    }

    public void StitchObj(GameObject mainObj, GameObject addObj)
    {
        SkinnedMeshRenderer[] bonedObjs = mainObj.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach(SkinnedMeshRenderer skinnedRend in bonedObjs)
        {
            ProcessBonedObject(skinnedRend, mainObj);
        }
    }

    private void ProcessBonedObject(SkinnedMeshRenderer thisRend, GameObject mainObj)
    {
        // Create SubObject
        GameObject newObj = new GameObject(thisRend.gameObject.name);
        newObj.transform.parent = mainObj.transform;
        // Add Renderer
        newObj.AddComponent<SkinnedMeshRenderer>();
        SkinnedMeshRenderer newRend = newObj.GetComponent<SkinnedMeshRenderer>();
        // Assemble Bone Structure
        Transform[] myBones = new Transform[thisRend.bones.Length];
        for(int i = 0; i < thisRend.bones.Length; i++)
        {
            myBones[i] = FindChildByName(thisRend.bones[i].name,mainObj.transform);
        }
        // Assemble Renderer
        newRend.bones = myBones;
        newRend.sharedMesh = thisRend.sharedMesh;
        newRend.materials = thisRend.materials;
    }

    private Transform FindChildByName(string thisName, Transform thisObj)
    {
        Transform returnObj;
        if(thisObj.name == thisName)
        {
            return thisObj.transform;
        }
        foreach(Transform child in thisObj)
        {
            returnObj = FindChildByName(thisName, child);
            if (returnObj)
            {
                return returnObj;
            }
        }
        return null;
    }
}
