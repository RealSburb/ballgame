﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Apply this to a Text component to keep track of time. Has a dependancy on the GameController for storing the energy.
/// </summary>
public class EnergyTimer : MonoBehaviour
{
    private IEnumerator coroutine; // if I ever need to stop it this is useful
    private Text energyReadout;

    void Start()
    {
        energyReadout = GetComponent<Text>();
        coroutine = Timer(2.0f);
        StartCoroutine(coroutine);
    }

    /// <summary>
    /// Bumps energy up by 1 every number of seconds waitTime
    /// </summary>
    /// <param name="waitTime"></param>
    /// <returns></returns>
    private IEnumerator Timer(float waitTime)
    {
        while (true)
        {
            //int eng = GameController_Script.control.getEnergy();
           // if (eng < 100)
            //{
                //GameController_Script.control.addEnergy();
                //energyReadout.text = GameController_Script.control.getEnergy() + "/100";
            //}
            //yield return new WaitForSeconds(waitTime);
        }
    }
}
