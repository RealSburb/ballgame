﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotMaker : MonoBehaviour { //InventoryFiller

    public Transform galaxyPrefab;
    public Transform catInfoPrefab;
    public Transform dividerPrefab;


    public void UpdateCatInventory()
    {
        // Clear old
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        // get new
        List<CatData> cats = GameController_Script.control.GetCats();
        foreach(CatData cat in cats)
        {
            // Instantiate Panel
            GameObject panel = Instantiate(galaxyPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
            // Parent Panel
            panel.transform.SetParent(this.transform, false); // false is important. Otherwise it resets the size and position

            // Set text
            panel.GetComponentInChildren<Text>().text = cat.Name;
            // Set catID
            CatSlot script = panel.GetComponent<CatSlot>();
            script.CatID = cat.IdNum;
        }
    }

    /// <summary>
    /// Create and set up CatInfo Prefab
    /// </summary>
    /// <param name="name"></param>
    /// <param name="attribute"></param>
    public void AddCattribute(string cattribute, string attribute)
    {
        GameObject slot = Instantiate(catInfoPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
        // Parent Panel
        slot.transform.SetParent(this.transform, false); // false is important. Otherwise it resets the size and position
        // Set text
        slot.GetComponent<CatInfoFabScript>().SetCattributes(cattribute,attribute);
    }

    /// <summary>
    /// Create and set up CatInfo Prefab
    /// </summary>
    public void AddGene(string gene, Color color)
    {
        GameObject slot = Instantiate(catInfoPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
        // Parent Panel
        slot.transform.SetParent(this.transform, false); // false is important. Otherwise it resets the size and position
        // Set text
        slot.GetComponent<CatInfoFabScript>().SetGene(gene, color);
    }

    /// <summary>
    /// Add empty slot
    /// </summary>
    public void AddDivider()
    {
        GameObject slot = Instantiate(dividerPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
        // Parent Panel
        slot.transform.SetParent(this.transform, false); // false is important. Otherwise it resets the size and position
    }

}
