﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles collisions and Controlling the cat via taps
/// </summary>
public class CatController : MonoBehaviour
{
    public enum Mode { Wander, Interact, Joystick, Stationary};
    public FloatingJoystick joystick;

    //public LayerMask layermask; // What can be clicked on to move
    //private Rigidbody rb; // reference to cat

    // Movement Type
    public Mode mode = Mode.Wander;

    // Utility
    private Animator anim;
    private WanderUtility wanderUtil;
    private SelectObject_Script selectable;

    // Attributes
    public float moveSpeed = 0.05f;
    public float joystickSpeed = 10f;
    public float rotationSpeed = 0.5f;
    public Transform headBone;
    ParticleSystem particles;

    // Movement
    private Vector3 targetPos;
    private Quaternion targetRot;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        wanderUtil = GetComponent<WanderUtility>();
        particles = GetComponentInChildren<ParticleSystem>();

        //TODO enable based on mode instead of disabling?s
        if (mode == Mode.Joystick) { 
            selectable = GetComponent<SelectObject_Script>();
            selectable.enabled = false;
        } 
    
    }

    // Update is called once per frame
    void Update()
    {
        if(mode == Mode.Joystick)
        {
            JoystickUpdate();
        }
        else if(mode == Mode.Wander)
        {
            anim.SetFloat("Speed", moveSpeed);
        }
        else if(mode == Mode.Interact)
        {
            //TODO
        }
        else if(mode == Mode.Stationary)
        {
            anim.SetFloat("Speed", 0);
        }
    }

    /// <summary>
    /// Update commands for Joystick
    /// </summary>
    private void JoystickUpdate()
    {
        Vector3 moveVector = (Vector3.right * joystick.Horizontal + Vector3.forward * joystick.Vertical);
        //float vectLen = moveVector.magnitude;
        //float limit = joystick.handleLimit;
        //float percentJoy = vectLen / limit;

        Debug.Log("Joystick: " + moveVector);
        if (moveVector != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(moveVector);
            transform.Translate(moveVector * joystickSpeed * Time.deltaTime, Space.World);
            anim.SetFloat("Speed", moveSpeed);
        }
        else
        {
            anim.SetFloat("Speed", 0);
        }
    }


    /// <summary>
    /// If colliding with prey activate that prey's collide behavior
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Monster" || col.gameObject.tag == "Prey") //TODO Prey is obsolete
        {
            Debug.Log("Collided with " + col.gameObject.name);
            MonsterBehavior p = col.gameObject.GetComponentInChildren<MonsterBehavior>();
            p.AttackedBehavior(gameObject);
        }
    }

    ///// <summary>
    ///// Check Explore for a selected monster. If one is selected the joystick is disabled and the creature is the target to move towards
    ///// </summary>
    //private void HandleSelection()
    //{
    //    Transform selected = ExploreControl.explore.GetSelectedMonster();
    //    if ( selected != null)
    //    {
    //        // Disable Joystick
    //        mode = Mode.Follow;

    //        // Set Location to move towards
    //        targetPos = selected.position;
    //        // Set direction to turn
    //        Vector3 lookAtTarget = new Vector3(
    //        targetPos.x - transform.position.x,
    //        0,
    //        targetPos.z - transform.position.z); // left and right differences in position
    //        targetRot = Quaternion.LookRotation(lookAtTarget); // angle to turn to look at positon
    //    }
    //    else
    //    {
    //        mode = Mode.Joystick;
    //    }
    //}

    ///// <summary>
    ///// Move to the position specified by playerRot.
    ///// </summary>
    //private void Move()
    //{
    //    transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, moveSpeed * Time.deltaTime); //Smoothly turn over time

    //    transform.position = Vector3.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime); // Move towards a point over time
    //}

    /// <summary>
    /// 
    /// </summary>
    void FixedUpdate()
    {
        if (mode == Mode.Joystick)
        {
        }

        if (mode == Mode.Wander)
        {
            MoveForwards();
            targetRot = wanderUtil.GetWanderAndCollisionRot();
            MoveRotation();
        }

        if (mode == Mode.Interact)
        {
            MoveRotation();
        }
    }

    /// <summary>
    /// Move to the position specified by playerRot.
    /// </summary>
    private void MoveForwards()
    {
        Vector3 m = Vector3.forward * (moveSpeed);
        transform.Translate(m);
    }

    /// <summary>
    /// Rotate to the target Rotation
    /// </summary>
    private void MoveRotation()
    {
        if (!transform.rotation.Equals(targetRot))
        {
            anim.SetFloat("Speed", moveSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * rotationSpeed);
        }
    }

    /// <summary>
    /// Start cat wandering
    /// </summary>
    public void StartWander()
    {
        mode = Mode.Wander;
    }

    /// <summary>
    /// Set interaction on or off
    /// </summary>
    public void ToggleInteract()
    {
        if (mode == Mode.Interact)
        {
            mode = Mode.Wander;
        }
        else
        {
            FacePlayer();
        }
    }

    /// <summary>
    /// Turn to face the camera
    /// </summary>
    public void FacePlayer()
    {
        mode = Mode.Interact;
        Transform target = Camera.main.transform;
        Vector3 targetPoint = new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(targetPoint, Vector3.up);
        targetRot = targetRotation;
    }

    private void OnMouseDown()
    {
        if(mode == Mode.Interact)
        {
            particles.Play();
            Debug.Log("Petting " + particles);
        }
    }
    private void OnMouseDrag()
    {
        //if (mode == Mode.Interact)
            //FacePlayer();
    }
    private void OnMouseUp()
    {
        if (mode == Mode.Interact)
            particles.Stop();
    }
}
