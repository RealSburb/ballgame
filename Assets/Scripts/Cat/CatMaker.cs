﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CatMaker {
    public int maxBlendShapes = 7;
    //public int maxMarkings = 0;
    private Dictionary<string, Material> materials; // TODO get from GameController
    public string[] randomNames = {"Beau", "Tyke", "Dill"};

    private CatData data;
    private string[] traits; //Contains all Traits;

    /// <summary>
    /// When called fill all data in the CatData class
    /// </summary>
    public CatData CreateCat(int id)
    {
        // if materials is unset set it
        if(materials == null)
        {
            materials = GameController_Script.control.getMarkings();
        }
        // if traits is unset set it
        if (traits == null || traits.Length == 0)
        {
            traits = GameController_Script.control.GetTraitKeys();
            /*string print = "";
            foreach(string s in traits)
            {
                print += s + " ";
            }
            Debug.Log("Possible traits: " + print);*/
        }

        data = new CatData(id);

        data.Gender = Random.Range(0, 2);
        data.Size = Random.Range(0.0F, 1.0F); // size modifier from 0 to 0.7 // 1 is added when drawn so 1 to 1.7
        data.SetBlendValues(generateBlendshapes());
        float v = Random.Range(0.0F,0.2F);
        data.SetBaseColor(new Color (v,v,v));
        data.SetEyeColor(new Color (Random.value, Random.value, Random.value ));
        // TODO set ear/paw/nose color
        data.Name = randomNames[Random.Range(0, randomNames.Length)];
        generateMaterials();
        GenerateTraits();
        GenerateStats();
        return data;
    }

    private void GenerateStats()
    {
        data.SetBaseAtk(UnityEngine.Random.Range(1, 6));
        data.SetBaseStl(UnityEngine.Random.Range(1, 6));
        data.SetBaseHp(UnityEngine.Random.Range(1, 6));
        data.SetBaseAcc(UnityEngine.Random.Range(1, 6));
    }

    /// <summary>
    /// Pick up to 3 random markings from the marking list and add them to the CatData -- generates 0 markings currently
    /// </summary>
    private void generateMaterials()
    {
        // Number of markings to generate
        int numMarks = Random.Range(0, 3); // 0 - 2 marking for now //TODO 0 to 3 markings for first gen cats
        string[] keys = new string[numMarks];
        float[,] colors = new float[numMarks,3];
        
        // Get list of markings that can be picked
        List<string> selectableKeys = Enumerable.ToList<string>(materials.Keys);
        
        // Pick Markings & Assn Colors
        for(int i = 0; i < numMarks; i++) // for number of markings
        {
            // Pick a key from 0-count-1
            int randKeyIndex = Random.Range(0,selectableKeys.Count);
            string key = selectableKeys[randKeyIndex];
            
            // set cat's key and color
            keys[i] = key;
            colors[i, 0] = Random.value; //r
            colors[i, 1] = Random.value; //g
            colors[i, 2] = Random.value; //b

            //Debug.Log("Marking" + i + " " + key);
            //remove key from list so it isn't picked again
            selectableKeys.Remove(key); 
        }
        data.SetMarkingsList(keys, colors);
    }

    /// <summary>
    /// Randomly generates values for each blendshape slider up to maxBlendshapes
    /// </summary>
    private float[] generateBlendshapes()
    {
        float[] blendValues = new float[maxBlendShapes];
        for(int i = 0; i < maxBlendShapes; i++)
        {
            blendValues[i] = Random.Range(0, 101);
        }
        return blendValues;
    }


    private void GenerateTraits()
    {
        // get list of possible traits
        GameController_Script.control.GetTraitKeys();
        // pick 1 or zero
        int num = Random.Range(0, 2);
        string[] newTraits = new string[num];
        float[,] colors = new float[num, 3];

        for (int i = 0; i < num; i++)
        {
            //Debug.Log("Num traits: " + traits.Length);
            int traitIndex = Random.Range(0, traits.Length);
            string t = traits[traitIndex];
            newTraits[i] = t;
            colors[i, 0] = Random.value; //r
            colors[i, 1] = Random.value; //g
            colors[i, 2] = Random.value; //b
        }
        //Debug.Log("Generated " + num + " traits" );
        data.SetTraitList(newTraits,colors);
    }

}
