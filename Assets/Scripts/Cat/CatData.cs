﻿using System;
using System.Runtime.Serialization;
using UnityEngine;


/// <summary>
/// Version 2.0
/// </summary>
[Serializable]
public class CatData
{
    private float versionID = 2f;

    //// RPG Attributes
    /// </summary>
    public string Name { get; set; }
    [OptionalField]
    public int level = 0;
    [OptionalField]
    private int[] baseStats = { 0, 0, 0, 0 }; // hp str acc stl
    [OptionalField]
    private int affinityID = -1; //TODO use for determining elemental type

    // Birthday
    private string dateAcquired; // use toString("0") to serialize
    private bool isBirthDay = false;

    // Determines Age/Lv
    private int powerUps = 0;     

    public int IdNum { get; set; }
    public int Gender { get; set; }
    public float Size { get; set; }

    private float[] blendshapeValues;

    private float[] eyeColor = new float[3];
    private float[] baseColor = new float[3];

    public string[] materialKeys = { };
    public float[,] colors = { };
    public string[] traitKeys = { };
    public float[,] traitColors = { };



    // Depreciated Land
    public string FavoriteFood { get; set; }
    [System.Obsolete("Use Powerups")]
    public int Points { get; set; }
    [OptionalField]
    public int mateID = -1; // -1 for none
    [OptionalField]
    private int rank = -1; // rank points. One with most rank points is leader?



    public CatData(int idNum)
    {
        this.IdNum = idNum;

    }

    // ---- GET ---- //
    public RPGAttributes GetRPGAttr() {
        RPGAttributes attr = new RPGAttributes();
        attr.hp = baseStats[0];
        attr.remainingHp = baseStats[0];
        attr.strength = baseStats[1];
        attr.accuracy = baseStats[2];
        attr.stealth = baseStats[3];
        attr.level = level;
        attr.name = Name;
        return attr;
    }
    public Color GetEyeColor() { return new Color(eyeColor[0], eyeColor[1], eyeColor[2]); }
    public Color GetBaseColor() { return new Color(baseColor[0], baseColor[1], baseColor[2]); }
    public float[] GetEyeColorFloats() { return new float[] { eyeColor[0], eyeColor[1], eyeColor[2] }; }
    public float[] GetBaseColorFloats() { return new float[] { baseColor[0], baseColor[1], baseColor[2] }; }
    public float[] GetBlendValues() { return blendshapeValues.Clone() as float[]; }
    public string[] GetMarkingKeys() { return materialKeys.Clone() as string[]; }
    public float[,] GetMarkingColors() { return colors.Clone() as float[,]; }
    public string[] GetTraitKeys() { return traitKeys.Clone() as string[]; }
    public float[,] GetTraitColors() { return traitColors.Clone() as float[,]; }
    public float GetVersionID() { return versionID; }
    public int GetPowerups() { return powerUps; }
    public int[] GetBaseStats() { return baseStats; }
    public int GetBaseHp() { return baseStats[0]; }
    public int GetBaseStr() { return baseStats[1]; }
    public int GetBaseAcc() { return baseStats[2]; }
    public int GetBaseStl() { return baseStats[3]; }
    public int GetRankId() { return rank; }
    public int GetMateId() { return mateID; }
    public int GetAffinityId() { return affinityID; }
    public float CalculateSizePercent()
    {
        float adultAge = 200;
        if(powerUps >= adultAge)
        {
            return 1;
        }
        else
        {
            return powerUps / adultAge;
        }
    }
    public int GetLevel() { return level; }

    // ---- SET ---- //
    public void SetEyeColor(Color color) {
        eyeColor[0] = color.r;
        eyeColor[1] = color.g;
        eyeColor[2] = color.b;
    }
    public void SetEyeColor(float[] color){
        eyeColor[0] = color[0];
        eyeColor[1] = color[1];
        eyeColor[2] = color[2];
    }

    public void SetBaseColor(Color color) {
        baseColor[0] = color.r;
        baseColor[1] = color.g;
        baseColor[2] = color.b;
    }
    public void SetBaseColor(float[] color)
    {
        baseColor[0] = color[0];
        baseColor[1] = color[1];
        baseColor[2] = color[2];
    }
    public void SetBlendValues(float[] array) { blendshapeValues = array.Clone() as float[]; }

    public void SetMarkingsList(string[] keys, float[,] colors)
    {
        materialKeys = keys.Clone() as string[];
        this.colors = colors.Clone() as float[,];
    }
    public void SetTraitList(string[] keys, float[,] colors)
    {
        traitKeys = keys.Clone() as string[];
        traitColors = colors.Clone() as float[,];
    }
    public void SetBaseHp(int hp) { baseStats[0] = hp; }
    public void SetBaseAtk(int atk) { baseStats[1] = atk; }
    public void SetBaseAcc(int acc) { baseStats[2] = acc; }
    public void SetBaseStl(int stl) { baseStats[3] = stl; }
    public void SetRankId(int rank) { this.rank = rank; } //TODO Remove
    public void SetMateId(int mate) { mateID = mate; } //TODO Remove
    public void SetAffinityId(int affin) { affinityID = affin; } //TODO Refactor into Element

    // ---- MODIFY ---- //
    public void PowerUp() { powerUps++; }
    public void PowerUp(int amt) { powerUps+=amt; }
    public void LevelUp() { level++; }

    // ---- UTILITY ---- //
    public bool IsAdult() { return level >= 6; }

    // ---- Clean up 
    [OnDeserializing]
    private void SetValues(StreamingContext sc)
    {
        //TODO Update to RPGAttr standards
        if(baseStats == null)
        {
            baseStats = new int[4];
            for(int i = 0; i< baseStats.Length; i++)
            {
            baseStats[i] = UnityEngine.Random.Range(1, 6);
            }
        }

        if(dateAcquired == null)
        {
            dateAcquired = System.DateTime.Now.ToString();
            // isBirthday is set to false by default
            //TODO random horoscope/element
        }
    }
}