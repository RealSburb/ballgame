﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Assign characteristics from catdata to a model this script is attached to
/// </summary>
public class CatInstance : MonoBehaviour {
    public GameObject model;
    public CatData data = new CatData(-1); //TODO for debug
    float adultLevel = 6;

    /// <summary>
    /// Apply CatData to this instance
    /// </summary>
    /// <param name="inDat"></param>
    public void setCatData(CatData inDat)
    {
        data = inDat;
        applySize();
        //applyBlendShapes();
        applyMaterials();
        applyTraits();
    }

    /// <summary>
    /// Applies the size modifier from the cata data relative to the age of the cat. 0.5 is the smallest increase in size possible.
    /// </summary>
    public void applySize()
    {
        float minIncr = 0.7f;
        float kittenSize = 0.5f;

        float lv = data.GetLevel();         // 0.0 - 1.0f
        float adultSize = data.Size + minIncr;  // if modifier is 0 then size is 0.7
        //Debug.Log("AdultSize " + adultSize);
        float modSize;
        if(lv >= adultLevel) // is an adult
        {
            modSize = adultSize;     // adult is 0.7 + (0.0, 1.0)
        }
        else
        {                           // base + (percent completion * (full size - base))
            modSize = kittenSize + ((lv/adultLevel) * (adultSize-kittenSize)); // 0.5 + %adult * remainderSize
        }

        modSize *= 0.75f;
        // Apply Size
        transform.localScale = new Vector3(modSize, modSize, modSize);
    }

    void applyBlendShapes()
    {
        SkinnedMeshRenderer meshRend = model.GetComponent<SkinnedMeshRenderer>();
        if (meshRend != null) {
            float[] bv = data.GetBlendValues();
            for (int i = 0; i < bv.Length; i++)
            {
                meshRend.SetBlendShapeWeight(i, bv[i]);
            }
        }
        else{
            print("MeshRend is null in CatInstance");
        }
    }

    void applyMaterials()
    {
        SkinnedMeshRenderer meshRend = model.GetComponent<SkinnedMeshRenderer>();

        //0 base    //1 eyes    //2Nose/Paws/Ears
        Material[] defMaterials = meshRend.materials; 
        // Set Base colors
        defMaterials[0].color = data.GetBaseColor();    // base
        defMaterials[1].color = data.GetEyeColor();     // eyes
        defMaterials[1].SetColor("_EmissionColor", data.GetEyeColor());
        defMaterials[2].color = data.GetEyeColor();     // Nose/Paws/Ears   //TODO give it's own color in cat maker

        // Get Marking keys and colors
        string[] keys = data.GetMarkingKeys();
        float[,] colors = data.GetMarkingColors();

        // Default materials
        int offset = 3; // so the base color, eyes, and ears don't get overwritten
        int numMaterials = offset + keys.Length;
        Material[] newMaterials = new Material[numMaterials];
        newMaterials[0] = defMaterials[0];
        newMaterials[1] = defMaterials[1];
        newMaterials[2] = defMaterials[2];

        // Get Marks from array
        //int inc = 2;
        for(int i=0; i<keys.Length;i++)
        {
            // get material from GC
            Material m = GameController_Script.control.getMaterial(keys[i]);
            if (m != null) // returns null if marking could not be found. Happens with pre-alpha cats
            {
                // Make color
                Color c = new Color(colors[i, 0], colors[i, 1], colors[i, 2]);
                // set color
                m.color = c;
                // add material to list
                newMaterials[i + offset] = m;
            }
        }
        // Assign
        meshRend.materials = newMaterials;
    }

    /// <summary>
    /// Turn on all relevant Traits
    /// </summary>
    private void applyTraits()
    {
        GetComponentInChildren<TraitHandler>().TurnOnTraits(data);
    }

    public int getCatID()
    {
        return data.IdNum;
    }

    public CatData getCatData()
    {
        return data;
    }
}
