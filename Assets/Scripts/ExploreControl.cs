﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExploreControl : MonoBehaviour
{
    public static ExploreControl explore;
    
    // Cat Prefab
    public CatInstance cat;

    
    // Editor
    public Transform dropPrefab;    //Drop Object Prefab
    public Transform fadeOutTextPrefab; // Fading text Prefab
    public GameObject questPanel;   //Quest Panel reference
    public Text points;
    public Transform questPrefab;   //Quest Slot Prefab
    public Level[] levels;
    public GameObject completionPopup;
    public GameObject joystick;
    public Transform starParticlePrefab;

    // Current Level

    private Level levelAttr;
    public GameObject targetProjector; // Projector for targeting monsters
    private Transform selectedMonster;

    // Quests
    private Dictionary<string, Quest> quests = new Dictionary<string, Quest>(); //Holds all Active Quests
    private Dictionary<string, GameObject> questSlots = new Dictionary<string, GameObject>(); //Holds all slot instances

    // All Prey Prefabs
    //public Transform[] preyPrefabs;
    //private Dictionary<string, Transform> preyDict;

    // All Drop Sprites
    public Sprite[] allDrops; //Array of all drop objects //TODO reconsider this and the drop dict
    private Dictionary<string, Sprite> dropDict; //Searchable Dictionary of all drops

    // Spawn Area
    public GameObject spawnArea;
    private Vector3 center;
    private Vector3 size;
    private float respawnRate = 20; // seconds


    private void LateUpdate()
    {
        //Debug.Log("mon: " + selectedMonster);
        if(selectedMonster != null)
        {
            //Debug.Log("Upderp");
            if (!targetProjector.activeSelf)
            {
                targetProjector.SetActive(true);
            }
            Vector3 projectorPos = targetProjector.transform.position;
            Vector3 monsterPos = selectedMonster.position;
            targetProjector.transform.position = new Vector3(monsterPos.x, projectorPos.y, monsterPos.z);
        }
        else if(targetProjector.activeSelf) // if monster is null and the projector is on turn it off
        {
            targetProjector.SetActive(false);
        }
    }

    /// <summary>
    /// Set the selected monster
    /// </summary>
    /// <param name="mon"></param>
    public void SetSelectedMonster(GameObject mon)
    {
        if(selectedMonster == mon)
        {
            selectedMonster = null;
        }
        else
        {
            selectedMonster = mon.transform;
        }
    }

    /// <summary>
    /// Get the selected monster's Transform.
    /// </summary>
    /// <returns></returns>
    public Transform GetSelectedMonster()
    {
        return selectedMonster;
    }

    //public List<Transform> spawnableObjects;
    private void Awake()
    {
        //Singleton
        if (explore == null)
        {
            explore = this;
        }
        else
        {
            Destroy(gameObject);
        }
        // Setup Level
        GetLevel();
        SetupLevel();

        //Spawn some Creatures
        if (spawnArea != null)
        {
            TranslateSpawnArea();
            SpawnInitialPrey();
        }
        points.text = GameController_Script.control.getPlayerPoints()+"";
    }

    /// <summary>
    /// Spawn 50 creatures
    /// </summary>
    private void SpawnInitialPrey()
    {
        // Get all Creatures
        Transform[] levelPrey = levelAttr.creatures;

        for(int i =0; i < 50; i++)
        {
            SpawnNumCreaturesWithinArea(1, levelPrey[i % levelPrey.Length]); // Spawn one of each creature until run out of creatures
        }

        //TODO split the total amt of creatures by the percent of that creature stored in the level data
        //TODO make lower tier monsters spawn more often
        //TODO wild cats
        //GameController_Script.control.addCatAtPosition(GenerateSpawnPoint()); //TODO add cat, needs lots of work
    }

    //TODO turn back on
    /// <summary>
    /// Add a new spawn to the map.
    /// </summary>
    public void NewSpawn() //TODO abide by percent chance spawn
    {
        Transform[] levelPrey = levelAttr.creatures;
        Transform pick = levelPrey[UnityEngine.Random.Range(0, levelPrey.Length)];
        SpawnNumCreaturesWithinArea(1, pick);
    }



    // Start is called before the first frame update
    void Start()
    {
        GameController_Script gc = GameController_Script.control;
        CatData selectedData = gc.getSelectedCatData();
        GameObject catObj = cat.gameObject;
        // Set the CatData of the Explore model to the selected cat
        cat.setCatData(selectedData);
        // Set the gameobject on the explore screen as the selected cat object in the game controller
        gc.setSelectedCat(catObj);
        gc.setCatObj(selectedData.IdNum,catObj);

        // Generate Drops Dictionary
        GenerateDropsDict();
        //GeneratePreyDict();

        // Create some quests and fill the panel
        GenerateLevelBasedQuests();

        // Setup Scenery
        SetupLevel();
    }

    /// <summary>
    /// Create a singular drop at a position // DEPRECIATED
    /// </summary>
    /// <param name="dropName"></param>
    /// <param name="transform"></param>
    public void CreateDropAtPos(string dropName, Transform transform)
    {
        GameObject dropInst = Instantiate(dropPrefab, transform.position, transform.rotation).gameObject;
        dropInst.GetComponent<Pickup>().buildPickup(dropName, dropDict[dropName]);
    }

    /// <summary>
    /// Create drop from list
    /// </summary>
    /// <param name="potDrop"></param>
    /// <param name="transform"></param>
    public void CreateDropAtPos(string[] dropList, Transform transform)
    {
        // If list is empty drop nothing
        if (dropList == null || dropList.Length <= 0)
        {
            return; // Do nothing
        }

        String drop = null;
        String newMark = null;
        Vector3 position = transform.position;
        Quaternion rotation = transform.rotation;

        if (UnityEngine.Random.Range(0, 20) == 0) // 1 in 20 is a Marking
        {
            drop = "Marking";
            // Generate Marking
            newMark = GameController_Script.control.GenRandomMark();
        }
        else
        {
            // pick from list
            drop = dropList[UnityEngine.Random.Range(0, dropList.Length)];
        }

        // Create Drop and set Sprite
        GameObject dropInst = Instantiate(dropPrefab, position, rotation).gameObject;
        dropInst.GetComponent<Pickup>().buildPickup(drop, dropDict[drop]);

        if(drop == "Marking")
        {
            // Create and set text
            Instantiate(fadeOutTextPrefab, position, rotation).GetComponent<FadeOutText>().SetText(newMark);
            Debug.Log("Spawned " + newMark);
            // Check off marking
            GameController_Script.control.CheckMarking(newMark);
        }
    }

    /// <summary>
    /// Build dictionary from list of Drops
    /// </summary>
    private void GenerateDropsDict()
    {
        dropDict = new Dictionary<string, Sprite>();
        for (int i = 0; i < allDrops.Length; i++)
        {
            Sprite s = allDrops[i];
            dropDict.Add(s.name,s);
        }
    }

    ///// <summary>
    ///// Build dictionary from list of Prey
    ///// </summary>
    //private void GeneratePreyDict()
    //{
    //    preyDict = new Dictionary<string, Transform>();
    //    for (int i = 0; i < preyPrefabs.Length; i++)
    //    {
    //        Transform t = preyPrefabs[i];
    //        Debug.Log("Prey Name: " + t.name);
    //        preyDict.Add(t.name, t);
    //    }
    //}



    private void Update()
    {
        points.text = GameController_Script.control.getPlayerPoints() + "";
    }

    /// <summary>
    /// Create a number of creatures at a position //Possibly Unused
    /// </summary>
    /// <param name="num"></param>
    /// <param name="obj"></param>
    private void SpawnNumCreaturesWithinArea(int num, Transform obj)
    {

        for (int i = 0; i < num; i++)
        {
            // Generate Random position within box
            Vector3 spawnPoint = center + new Vector3(
               (UnityEngine.Random.value - 0.5f) * size.x,
               0,
               (UnityEngine.Random.value - 0.5f) * size.z);

            // Set Rotation
            Quaternion rotation = UnityEngine.Random.rotation;
            rotation.x = 0;
            rotation.z = 0;

            //TODO set on ground // Use raycast from 500 up at spawnPoint

            // Spawn Creature
            GameObject spawnObj = Instantiate(obj, spawnPoint, rotation).gameObject;
            //Instantiate(starParticlePrefab, spawnObj.transform.position, Quaternion.identity);
            //spawnObj.GetComponent<Renderer>().material.color = Color.blue;
        }
    }

    private Vector3 GenerateSpawnPoint()
    {
        return center + new Vector3(
               (UnityEngine.Random.value - 0.5f) * size.x,
               0,
               (UnityEngine.Random.value - 0.5f) * size.z);
    }



    /// <summary>
    /// Translate the game object to center and size for spawn within area
    /// </summary>
    private void TranslateSpawnArea()
    {
        //Fetch the Collider from the GameObject
        Collider col = spawnArea.GetComponent<Collider>();
        //Fetch the center of the Collider volume
        center  = col.bounds.center;
        //Fetch the size of the Collider volume
        size = col.bounds.size;
    }

    ///// <summary>
    ///// TODO create a number of quests and put them in the quest panel
    ///// </summary>
    //private void GenerateQuests()
    //{
    //    //TODO Pick random items and numbers
    //    for(int i =0; i < 5; i++)
    //    {
    //        //Get drop
    //        Item drop = drops[i];
    //        //Make new Quest
    //        Quest q = new Quest();
    //        //Assign amt to name
    //        q.amtNeeded = UnityEngine.Random.Range(1, 21);
    //        //Add quest to dictionary
    //        quests[drop.name] = q;
    //    }
    //    //Apply to questPanel
    //    foreach(string n in quests.Keys)
    //    {
    //        // Get Quest
    //        Quest q = quests[n];
    //        // Instantiate Panel
    //        GameObject panel = Instantiate(questPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
    //        // Set attributes
    //        QuestItemSlot qis = panel.GetComponent<QuestItemSlot>();
    //        qis.setText(q.amtNeeded);
    //        qis.SetSprite(dropDict[n]);
    //        // Parent Panel
    //        panel.transform.SetParent(questPanel.transform, false); // false is important. Otherwise it resets the size and position
    //        // add to dictionary
    //        questSlots[n] = panel;
    //    }

    //}

    /// <summary>
    ///  Set text on Slot to new value
    /// </summary>
    private void UpdateQuestSlot(string itmName)
    {
        Quest q = quests[itmName];
        QuestItemSlot slot = questSlots[itmName].GetComponent<QuestItemSlot>();
        slot.UpdateText(q);
    }

    /// <summary>
    /// TODO Check quests to see if the item just picked up fits a quest. Otherwise reward a star.
    /// </summary>
    /// <param name="name"></param>
    public void CheckQuests(string itmName)
    {
        GameController_Script gc = GameController_Script.control;
        // Do not check marking because already added
        if (quests.ContainsKey(itmName))
        {
            if (quests[itmName].incrmtAndCheckDone()) // If it is done
            {
                
                // Calculate award
                int amt = quests[itmName].amtNeeded*2; // Award is 2x the amount needed to complete the quest
                GameObject qObj = questSlots[itmName];
                // Remove from list         //TODO particle explosion
                quests.Remove(itmName);
                questSlots.Remove(itmName);
                // Destroy GameObj
                Destroy(qObj);
                // Award points
                gc.AddPlayerPoints(amt);

                if(quests.Count == 0)
                {
                    // Activate Particles
                    Transform catTrans = GameController_Script.control.getSelectedCat().transform;
                    Instantiate(starParticlePrefab, catTrans.position, Quaternion.identity);
                    // +1 Level
                    GameController_Script.control.LevelUpCat();
                    // Show dialog
                    ToggleJoystick();
                    completionPopup.SetActive(true);
                }
            }
            else // if not done
            {
                UpdateQuestSlot(itmName); // Only update the slot if not done
            }
        }

        //Awards a single point for any pickup
        gc.AddPlayerPoints(1);
        
    }

    /// <summary>
    /// Get data from Level and load it's attributes
    /// </summary>
    public void SetupLevel()
    {
        // Skybox
        RenderSettings.skybox = levelAttr.skybox;
        // Terrain
        levelAttr.terrain.gameObject.SetActive(true);
        // TODO Generate Quests

        // Update lighting
        DynamicGI.UpdateEnvironment();
    }

    /// <summary>
    /// Determine which level to load based on the cat's level
    /// </summary>
    public void GetLevel()
    {
        // TODO get level depending on cat level
        int catLevel = GameController_Script.control.getSelectedCatData().GetLevel();
        if(catLevel < levels.Length)
        {
            levelAttr = levels[catLevel];
        }
        else
        {
            levelAttr = levels[levels.Length-1];
        }
    }

    /// <summary>
    /// Generate quests based on items found on that level
    /// </summary>
    public void GenerateLevelBasedQuests()
    {
        // Generate Drop List from level
        List<String> dropList = levelAttr.GetDroplist();
        int maxNum = 30/dropList.Count;
        // if there are 30 drops only up to 1 of each will be needed
        // if there are 2 drops 7-15 are needed
        //(range is 1/2 to 1)

        // Make sure there are at least 5 drops to choose from
        int maxQuests = 5;
        //Debug.Log("Droplist count: " + dropList.Count);
        if(dropList.Count < 5) 
        {
            maxQuests = dropList.Count;
        }

        // Pick 5 random items in quantities of 10-20
        for (int i = 0; i < maxQuests; i++)
        {
            // Pick an item from the array of drops
            String dropName = dropList[UnityEngine.Random.Range(0, dropList.Count)];
            dropList.Remove(dropName); // Remove so it doesn't get picked again
        
            //Make new Quest
            Quest q = new Quest();
            //Assign amt to name
            q.amtNeeded = UnityEngine.Random.Range(maxNum/2, maxNum);
            //Add quest to dictionary
            quests[dropName] = q;
        }

        //Apply to questPanel
        foreach (string n in quests.Keys)
        {
            // Get Quest
            Quest q = quests[n];
            // Instantiate Panel
            GameObject panel = Instantiate(questPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
            // Set attributes
            QuestItemSlot qis = panel.GetComponent<QuestItemSlot>();
            qis.setText(q.amtNeeded);
            qis.SetSprite(dropDict[n]);
            // Parent Panel
            panel.transform.SetParent(questPanel.transform, false); // false is important. Otherwise it resets the size and position
            // add to dictionary
            questSlots[n] = panel;
        }

    }

    /// <summary>
    /// Toggle the joystick for dialog
    /// </summary>
    public void ToggleJoystick()
    {
        if (joystick.activeSelf)
        {
            joystick.SetActive(false);
        }
        else
        {
            joystick.SetActive(true);
        }
    }
}


//[Serializable]
//public class Item
//{
//    public Sprite sprite;

//    public String GetName()
//    {
//        return sprite.name;
//    }
//}

public class Quest
{
    public int amtDone;
    public int amtNeeded;

    /// <summary>
    /// Increases amount completed and returns whether the quest is finished or not
    /// </summary>
    /// <returns></returns>
    public bool incrmtAndCheckDone()
    {
        amtDone++;
        if(amtDone >= amtNeeded)
        {
            return true;
        }
        return false;
    }
}

//TODO Use this to handle prey instead of just having an array of transforms?
//[Serializable]
//public class Spawnable
//{
//    public string name;
//    public Transform prefab;
//    public int spawnPercent;
//}