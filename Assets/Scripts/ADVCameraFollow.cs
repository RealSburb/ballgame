﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADVCameraFollow : MonoBehaviour {
    private Vector3 defaultPos;
    //private Vector3 defaultEuler;
    private Transform target;
    public float smoothSpeed = 0.125f; // between 0 and 1
    public Vector3 offsetPos;
    public Vector3 eulerOffset;
    public Transform debugCat;
    public bool debug = false;


    /// <summary>
    /// Set a default position to return to when nothing is selected
    /// </summary>
    void Start () {
        defaultPos = transform.position;
        //defaultEuler = transform.eulerAngles;
	}

    /// <summary>
    /// Update camera after all movement is done.
    /// </summary>
    private void LateUpdate()
    {
        if (!debug)
        {
            Vector3 newPos;
            Vector3 newEul;
            // If there is a target
            if (target != null)
            {
                newPos = target.position + offsetPos;
                newEul = eulerOffset;
                //transform.position = target.position + offsetPos;
                transform.position = Vector3.Lerp(transform.position, newPos, smoothSpeed);
                //transform.eulerAngles = eulerOffset;
            }
            else // if there is no target
            {
                if (transform.position != defaultPos) // if not set to default view
                {
                    //transform.position = defaultPos;
                    transform.position = Vector3.Lerp(transform.position, defaultPos, smoothSpeed);
                    //transform.eulerAngles = defaultEuler;
                }
            }
        }
        else
        {
            transform.position = debugCat.position + offsetPos;
        }
    }
	
	// Update is called once per frame
	void Update () {
            GameObject obj = GameController_Script.control.getSelectedCat();
            if (obj != null)
            {
                target = obj.transform;
            }
            else
            {
                target = null;
            }
	}
}
