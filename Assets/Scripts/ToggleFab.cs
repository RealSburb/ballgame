﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleFab : MonoBehaviour
{
    public Sprite toggleOn;
    public Sprite toggleOff;

    public Image toggleImage;
    public Text text;

    private bool isOn = false;

    public bool isChecked()
    {
        return isOn == true;
    }

    public void SetText(string t)
    {
        text.text = t;
    }

    private void ToggleOn()
    {
        Debug.Log("toggled");
        isOn = true;
        toggleImage.overrideSprite = toggleOn;
    }

    private void ToggleOff()
    {
        isOn = false;
        toggleImage.overrideSprite = toggleOff;
    }

    public void Toggle(bool dir)
    {
        if (dir)
        {
            ToggleOn();
        }
        else
        {
            ToggleOff();
        }
    }
}
