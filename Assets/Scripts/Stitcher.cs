﻿// Modifications by GameDevStudent. Original script was downloaded from here: https://github.com/masterprompt/ModelStitching 

using UnityEngine;
using System.Collections.Generic;

public class Stitcher
{
	/// <summary>
	/// Stitch clothing onto an avatar.  Both clothing and avatar must be instantiated however clothing may be destroyed after.
	/// </summary>
	/// <param name="sourceClothing"></param>
	/// <param name="targetAvatar"></param>
	/// <returns>Newly created clothing on avatar</returns>
    
	public GameObject Stitch (GameObject sourceClothing, GameObject targetAvatar)
	{
        Debug.Log("Stitcher Start");
        Debug.Log("sourceClothing: " + sourceClothing.name + " target: " + targetAvatar.name);
        TransformCatalog boneCatalog = new TransformCatalog (targetAvatar.transform);
		SkinnedMeshRenderer[] skinnedMeshRenderers = sourceClothing.GetComponentsInChildren<SkinnedMeshRenderer> ();
        Debug.Log("Part SMR size: " + skinnedMeshRenderers.Length);
		GameObject targetClothing = AddChild (sourceClothing, targetAvatar.transform);
        foreach (SkinnedMeshRenderer sourceRenderer in skinnedMeshRenderers) {
			SkinnedMeshRenderer targetRenderer = AddSkinnedMeshRenderer (sourceRenderer, targetClothing);
			targetRenderer.bones = TranslateTransforms (sourceRenderer.bones, boneCatalog);
            // debug

            string debug = "bones\n";
            foreach (Transform t in targetRenderer.bones)
            {
                debug += t.name + '\n';
            }
            Debug.Log(debug);

        }
        Debug.Log("Stitcher End");
        return targetClothing;
	}

	private GameObject AddChild (GameObject source, Transform parent)
	{
		GameObject target = new GameObject (source.name);
		target.transform.parent = parent;
		target.transform.localPosition = source.transform.localPosition;
		target.transform.localRotation = source.transform.localRotation;
		target.transform.localScale = source.transform.localScale;
		return target;
	}

	private SkinnedMeshRenderer AddSkinnedMeshRenderer (SkinnedMeshRenderer source, GameObject parent)
	{
		SkinnedMeshRenderer target = parent.AddComponent<SkinnedMeshRenderer> ();
		target.sharedMesh = source.sharedMesh;
		target.materials = source.materials;
		return target;
	}

	private Transform[] TranslateTransforms (Transform[] sources, TransformCatalog transformCatalog)
	{
        Debug.Log("Source Trans " + sources.Length + " catalog " + transformCatalog);
        Transform[] targets = new Transform[sources.Length];
		for (int index = 0; index < sources.Length; index++)
			targets [index] = DictionaryExtensions.Find (transformCatalog, sources [index].name);
		return targets;
	}


    #region TransformCatalog
	private class TransformCatalog : Dictionary<string, Transform>
	{
        #region Constructors
		public TransformCatalog (Transform transform)
		{
            Catalog (transform);
		}
        #endregion

        #region Catalog
		private void Catalog (Transform transform)
		{
            Debug.Log("Cataloging " + transform.name);
            if (ContainsKey(transform.name))
            {
                Debug.Log("Contains Key " + transform.name);
                Remove(transform.name); 
                Add(transform.name, transform);
            } 
            else
                Add(transform.name, transform);
            foreach (Transform child in transform)
				Catalog (child);
		}
        #endregion
	}
    #endregion


    #region DictionaryExtensions
	private class DictionaryExtensions
	{
		public static TValue Find<TKey, TValue> (Dictionary<TKey, TValue> source, TKey key)
		{
			TValue value;
			source.TryGetValue (key, out value);
			return value;
		}
	}
    #endregion

}

