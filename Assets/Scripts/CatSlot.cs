﻿
using UnityEngine;

public class CatSlot: MonoBehaviour{
    int catID;

    public int CatID
    {
        get
        {
            return catID;
        }

        set
        {
            catID = value;
        }
    }

    //public void DestroyCat()
    //{
    //    int selectedID = GameController_Script.control.getSelectedCatID();
    //    if(selectedID == CatID)
    //    {
    //        CatInfoPanel.catInfoPanel.deactivateInfoPanel();
    //    }
    //    GameController_Script.control.removeCat(this);
    //    //SlotMaker panel = GetComponentInParent<SlotMaker>(); //TODO what was this doing?
    //}

    public void ZoomToCat()
    {
        GameController_Script.control.setSelectedCat(catID);
        CatInfoPanel.catInfoPanel.ActivateInfoPanel();
    }

    public void OpenCatCard()
    {
        Debug.Log("Open Cat Card");
        GameController_Script.control.setSelectedCat(catID);
        PanelController.controller.OpenCatCard();
    }
}
