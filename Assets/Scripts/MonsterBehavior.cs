﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MonsterBehavior : MonoBehaviour
{
    public enum MovementType {Basic, Flee, Hostile, Stationary};

    // Editor Options
    public MovementType movement = MovementType.Basic;
    public int hp; // health points
    public int op; // offense points
    public int rating; // How hard a monster is and how many points to give on defeat
    public float speed = 0.05f;
    public string[] dropList;

    // Utility
    public bool adventureMode = true;
    public bool colorChanged = false;
    public Color defaultColor;

    public void Start()
    {
        if(movement == MovementType.Basic) // Add basic Wander Script
        {
            gameObject.AddComponent<Wander>();
            GetComponent<Wander>().setSpeed(speed);
        }
        if (movement == MovementType.Flee) // Add Flee Script
        {
            //TODO
        }
        if (movement == MovementType.Hostile) // Add Hostile Script
        {
            //TODO
        }
        if(movement == MovementType.Stationary)
        {
            //NOTHING
        }
    }

    /// <summary>
    /// Select monster so the projector can track it
    /// </summary>
    public void OnMouseUp()
    {
        //Debug.Log("Targeted " + gameObject.name);
        if (adventureMode)
        {
            ExploreControl.explore.SetSelectedMonster(gameObject);
        }
        //StartCoroutine(Die());
    }

    /// <summary>
    /// Called by a CatController when the cat collides during an attack animation
    /// </summary>
    /// <param name="catObj"></param>
    public void AttackedBehavior(GameObject catObj)
    {
        StartCoroutine(FlashColor());
        hp--; //TODO decriment by the cat's attack stat

        if(hp <= 0)
        {
            //Stop collisions immediately
            Rigidbody r = GetComponent<Rigidbody>();
            r.detectCollisions = false;

            // Get catInstance
            CatInstance catInst = catObj.GetComponent<CatInstance>();

            if (adventureMode)
            {
                // Tell Controller to Spawn drop
                ExploreControl.explore.CreateDropAtPos(dropList, transform);    
                // Replace Spawn
                ExploreControl.explore.NewSpawn();
            }

            // Award Kill // regardless of Scene
            GameController_Script.control.AddPlayerPoints(rating);
            GameController_Script.control.IncrementKills(catInst.getCatData());

            StartCoroutine(Die());
        }
    }

    IEnumerator FlashColor()
    {
        //Debug.Log("Color changed");
        Renderer r = GetComponent<Renderer>();
        if(r != null)
        {
            defaultColor = r.material.color;
            r.material.color = Color.red;
            //while(r.material.color != defaultColor) // If it doesn't get reset then reset it
            //{
            yield return new WaitForSeconds(0.5f);
            r.material.color = defaultColor;
            //}
        }
    }

    IEnumerator Die()
    {
        //TODO other movement types
        if(movement != MovementType.Stationary)
        {
            GetComponent<Wander>().enabled = false;
        }
        Rigidbody r = GetComponent<Rigidbody>();
        r.detectCollisions = false;
        yield return new WaitForSeconds(0.5f);
        //Debug.Log("dead");
        Destroy(gameObject);
    }

    private void Recoil()
    {
     // apply a force from collision   
    }
}
