﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawner : MonoBehaviour
{
    public List<Transform> spawnablePrey;
    private Vector3 center;
    private Vector3 size;
    public float delaySec = 1.0f;
    private IEnumerator coroutine;


    private void Awake()
    {
        TranslateSpawnArea();
        SpawnNumObjWithinArea(1, spawnablePrey[0]); //small bebe
        //SpawnNumObjWithinArea(1, spawnablePrey[1]); //big bebe
        coroutine = SpawnAfterDelay(delaySec);
        StartCoroutine(coroutine);
    }

    private void SpawnNumObjWithinArea(int num, Transform obj)
    {

        for (int i = 0; i < num; i++)
        {
            // Generate Random position within box
            Vector3 spawnPoint = center + new Vector3(
               (Random.value - 0.5f) * size.x,
               0.5f,
               (Random.value - 0.5f) * size.z);

            // Set Rotation
            Quaternion rotation = Random.rotation;
            rotation.x = 0;
            rotation.z = 0;

            //TODO set on ground // Use raycast from 500 up at spawnPoint

            // Spawn Creature
            GameObject spawnObj = Instantiate(obj, spawnPoint, rotation).gameObject;
            //spawnObj.GetComponent<Renderer>().material.color = Color.blue;
        }
    }

    private Vector3 GenerateSpawnPoint()
    {
        return center + new Vector3(
               (Random.value - 0.5f) * size.x,
               0,
               (Random.value - 0.5f) * size.z);
    }

    /// <summary>
    /// Translate the game object to center and size for spawn within area
    /// </summary>
    private void TranslateSpawnArea()
    {
        //Fetch the Collider from the GameObject
        Collider col = GetComponent<Collider>();
        //Fetch the center of the Collider volume
        center = col.bounds.center;
        //Fetch the size of the Collider volume
        size = col.bounds.size;
    }

    private IEnumerator SpawnAfterDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            SpawnNumObjWithinArea(1, spawnablePrey[0]);
        }
    }

}
