﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization;

/// <summary>
/// In charge of Data needed across all scenes such as CatData, CatInstances and Player Points
/// </summary>
public class GameController_Script : MonoBehaviour
{
    public static GameController_Script control;
    public Font font;

    // -- Global Prefabs --
    // -- Player Data --
    private int playerPoints = 20;
    //private bool[] mcheck = new bool[0];
    //private bool[] tcheck = new bool[0];
    private Dictionary<string, bool> markingChecklist = new Dictionary<string, bool>();
    private Dictionary<string, bool> traitChecklist = new Dictionary<string, bool>();

    // -- Genetics --
    public Dictionary<string, Material> markings;
    public Marking[] marks;
    private string[] traits;
    private int numPowerUpsToAdult = 100;

    // -- Generating Cats --
    public Transform catPrefab; //TODO Use this in all scenes
    public CatMaker catMaker = new CatMaker();

    // ---- Cat Management ----
    private List<CatData> cats = new List<CatData>();
    private Dictionary<int, GameObject> catObjs = new Dictionary<int, GameObject>(); // Dictionary used for destorying instances when a cat is removed
    private int numCats = 0; // Id Counter
    private CatData selectedCatData = null;

    // happens before start
    void Awake()
    {
        Debug.Log("GC Awake " + selectedCatData);
        // create Singleton
        if (control == null) // First time running
        {
            DontDestroyOnLoad(gameObject);
            control = this;
            // Set up Dictionaries
            GenerateMarkingDict();
            GeneratePossibleTraits();
            // Load after everything else is set up
            Load();
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Check off marking by name
    /// </summary>
    /// <param name="name"></param>
    public void CheckMarking(string name)
    {
        markingChecklist[name] = true;
    }

    /// <summary>
    /// Generate a Random Marking Key (From all markings locked or unlocked)
    /// </summary>
    /// <returns></returns>
    public string GenRandomMark()
    {
        string[] keys = markings.Keys.ToArray<string>();
        return keys[UnityEngine.Random.Range(0, keys.Length)];
    }

    /// <summary>
    /// Get keys from instantiated test cat
    /// </summary>
    private void GeneratePossibleTraits()
    {
        Transform instCat = Instantiate(catPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
        TraitHandler th = catPrefab.GetComponentInChildren<TraitHandler>();
        traits = th.GetTraitKeys();
        Destroy(instCat.gameObject);

        foreach (string name in traits) //setup but do not assign
        {
            traitChecklist.Add(name, false);
        }
    }

    /// <summary>
    /// Pass trait key array to CatMaker
    /// </summary>
    /// <returns></returns>
    public string[] GetTraitKeys()
    {
        return traits;
    }

    /// <summary>
    /// Pass marking checklist to GuideBook
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, bool> GetMarkingChecklist()
    {
        return markingChecklist;
    }
    /// <summary>
    /// Pass trait checklist to GuideBook
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, bool> GetTraitChecklist()
    {
        return traitChecklist;
    }

    /// <summary>
    /// Build checklist and material dictionaries from list of Markings
    /// </summary>
    private void GenerateMarkingDict()
    {
        markings = new Dictionary<string, Material>();
        markingChecklist = new Dictionary<string, bool>();

        for (int i = 0; i < marks.Length; i++)
        {
            Marking m = marks[i];
            markings.Add(m.GetName(), m.material);
            markingChecklist.Add(m.GetName(), false); // setup but do not assign
        }
    }


    /// <summary>
    /// Return a copy of the marking Dictionary. Used by CatMaker. TODO change CatMaker to only request the keys not the dict
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, Material> getMarkings()
    {
        return new Dictionary<string, Material>(markings);
    }

    /// <summary>
    /// Get the material associated with a marking name. Used by CatInstance.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public Material getMaterial(String name)
    {
        // Valid marking
        if (markings.ContainsKey(name))
        {
            Material m = new Material(markings[name]);
            return m;
        }
        // For older cats
        String capsName = char.ToUpper(name[0]) + name.Substring(1);
        if (markings.ContainsKey(capsName))
        {
            Material m = new Material(markings[capsName]);
            return m;
        }
        // If marking just doesn't show up
        return null;
    }

    public void NewGame()
    {
        playerPoints = 20;
        WipeCatArrays();
        WipeCatData();
        numCats = 0;
        if (CatInfoPanel.catInfoPanel.IsOpen())
        {
            CatInfoPanel.catInfoPanel.deactivateInfoPanel();
        }
    }

    /// <summary>
    /// Add or remove points
    /// </summary>
    /// <param name="amt"></param>
    public void addPlayerPoints(int amt)
    {
        playerPoints += amt;
    }

    /// <summary>
    /// Get current point value
    /// </summary>
    /// <returns></returns>
    public int getPlayerPoints()
    {
        return playerPoints;
    }

    /// <summary>
    /// Clear all data for reloading.
    /// </summary>
    public void WipeCatArraysComplete()
    {
        selectedCatData = null;
        WipeCatArrays();
        if (CatInfoPanel.catInfoPanel != null)
        {
            CatInfoPanel.catInfoPanel.deactivateInfoPanel(); //If panel is open close it
        }
    }

    /// <summary>
    /// Clear all cat Arrays except CatData
    /// </summary>
    private void WipeCatArrays()
    {
        // Deselect selected cat
        //selectedCatData = null;

        // Kill old dict instances
        List<GameObject> objs = catObjs.Values.ToList<GameObject>();
        foreach (GameObject g in objs)
        {
            Destroy(g);
        }

        // Clear Dictionary
        catObjs = new Dictionary<int, GameObject>();
    }

    /// <summary>
    /// Clear CatData Array
    /// </summary>
    private void WipeCatData()
    {
        cats = new List<CatData>();
    }

    /// <summary>
    /// Reloads cats into main scene after switching scenes
    /// </summary>
    public void ReinstantiateCats()
    {
        // Clear old data
        WipeCatArrays();

        // Re-add each cat
        int x = -20;
        int z = 20;
        foreach (CatData cat in cats) // -20, to 20 is screen size
        {
            AddCatToScene(cat, new Vector3(x, 0, z));
            x += 5;
            if (x >= 20)
            {
                x = -20;
                z -= 5;
            }
        }
        // If a cat was selected prior open the catinfopanel
        if (getSelectedCat() != null)
        {
            CatInfoPanel.catInfoPanel.ActivateInfoPanel();
        }
    }

    /// <summary>
    /// PowerUp the selected cat once
    /// </summary>
    public void IncrementKills(CatData data)
    {
        data.PowerUp();
        if (data.GetPowerups() <= numPowerUpsToAdult)
        {
            catObjs[data.IdNum].GetComponent<CatInstance>().applySize(); //TODO This will need to apply everything at certain levels
        }
    }

    /// <summary>
    /// LevelUp the selected cat once and apply new size
    /// </summary>
    public void LevelUpCat()
    {
        selectedCatData.LevelUp();
        getSelectedCat().GetComponent<CatInstance>().applySize();
    }

    ///// <summary>
    ///// PowerUp the selected cat by an increment
    ///// </summary>
    //public void PowerUpCat(CatData data, int amt)
    //{
    //    data.PowerUp(amt);
    //    if (data.GetPowerups() <= numPowerUpsToAdult)
    //    {
    //        catObjs[data.IdNum].GetComponent<CatInstance>().applySize(); //TODO This will need to apply everything at certain levels
    //    }
    //}

    ///// <summary>
    ///// Grow Cat to Adult if can afford
    ///// </summary>
    //public void BuyPowerUp()
    //{
    //    int amtToMax = 100 - selectedCatData.GetPowerups();
    //    if (amtToMax > 0 && playerPoints > amtToMax)
    //    {
    //        playerPoints -= amtToMax;
    //        //(selectedCatData, amtToMax);
    //    }
    //    //TODO Error Message
    //}

    /// <summary>
    /// PoweUp the selected cat
    /// </summary>
    public void PowerUpSelectedCat()
    {
        IncrementKills(selectedCatData);
    }

    /// <summary>
    /// Get the PowerUp value of the selected cat
    /// </summary>
    /// <returns></returns>
    public int getPowerups()
    {
        return selectedCatData.GetPowerups();
    }

    /// <summary>
    /// Used by Breeding Controller to add a kitten to the set of cats
    /// </summary>
    /// <param name="kitten"></param>
    public void addKitten(CatData kitten)
    { //TODO focus on kitten
        // Create CatData and add to List
        cats.Add(kitten);
        selectedCatData = kitten;
        Debug.Log("Selected: " + selectedCatData.Name);
    }

    /// <summary>
    /// Used by Breeding Controller to retrieve the next CatID
    /// </summary>
    /// <returns></returns>
    public int GetAndIncrementID()
    {
        return numCats++;
    }

    /// <summary>
    /// Create a new CatData and add the new cat to the scene at origin
    /// </summary>
    public void AddNewCatToScene()
    {
        AddNewCatToScene(new Vector3(0, 0, 0));
    }

    /// <summary>
    /// Used for adding a wild cat. TODO implement
    /// </summary>
    /// <param name="pos"></param>
    public void AddNewCatToScene(Vector3 pos)
    {
        // Create CatData and add to List
        CatData newCat = catMaker.CreateCat(numCats);
        cats.Add(newCat);
        AddCatToScene(newCat, pos);
        // Increment numCats
        numCats++;
    }


    /// <summary>
    /// Instantiate a Cat Object with a CatData parameter at a position
    /// </summary>
    /// <param name="catData"></param>
    /// <param name="position"></param>
    private void AddCatToScene(CatData catData, Vector3 position)
    {
        // instantiate cat at position
        Transform catTransform = Instantiate(catPrefab, position, Quaternion.identity);
        // Store Prefab Instance
        GameObject catObj = catTransform.gameObject;
        Debug.Log("Adding catID" + catData.IdNum);
        catObjs.Add(catData.IdNum, catObj);
        // Apply CatData to Prefab
        CatInstance catInst = catObj.GetComponentInChildren<CatInstance>();
        catInst.setCatData(catData);
    }


    /// <summary>
    /// Release a cat back into the wild from the menu.
    /// </summary>
    public void removeCat(CatSlot panelScript)
    {
        int idRem = panelScript.CatID;
        // Find which cat got removed and return 1/3 of the powerups + 10
        int returnPts = (FindCatData(idRem).GetPowerups() / 3) + 10;
        AddPlayerPoints(returnPts);
        // If the removed cat is the selected one
        if (selectedCatData != null && selectedCatData.IdNum == idRem)
        {
            selectedCatData = null;     // Deselect
        }
        // Remove from world
        Destroy(catObjs[idRem]);
        // Remove from array
        findAndRemoveCatData(idRem);
        // Remove from panel
        Destroy(panelScript.gameObject);
    }

    /// <summary>
    /// Remove CatData by ID. Used to remove from the Galaxy Panel.
    /// </summary>
    /// <param name="idRem"></param>
    private void findAndRemoveCatData(int idRem)
    {
        int index = 0;
        foreach (CatData c in cats)
        {
            if (c.IdNum == idRem)
            {
                cats.RemoveAt(index);
                break;
            }
            index++;
        }
    }

    /// <summary>
    /// Find a catdata by ID. Used by the Galaxy panel to get powerups.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private CatData FindCatData(int id)
    {
        int index = 0;
        foreach (CatData c in cats)
        {
            if (c.IdNum == id)
            {
                return cats[index];
            }
            index++;
        }
        return null;
    }

    //TODO when back from Adventure mode what happens?
    /// <summary>
    /// Used to set the object of a cat in Adventure/Explore mode
    /// </summary>
    public void setCatObj(int id, GameObject obj)
    {
        catObjs[id] = obj;
    }

    /// <summary>
    /// Set selected cat by GameObject
    /// </summary>
    /// <param name="cat"></param>
    public void setSelectedCat(GameObject cat)
    {
        if (cat == null) //SetNull for no selected cat
        {
            selectedCatData = null;
        }
        else
        {
            selectedCatData = cat.GetComponent<CatInstance>().getCatData();
            // Reload InfoPanel
            UpdateCatInfoPanel();
        }
    }

    /// <summary>
    /// Method to update the CatInfoPanel if it is in the scene
    /// </summary>
    public void UpdateCatInfoPanel()
    {
        if (SceneManager.GetActiveScene().name == "Main") //TODO possibly set a scene name enum whenever the scene is changed instead
        {
            CatInfoPanel.catInfoPanel.reloadTab();
        }
    }

    /// <summary>
    /// Set selected cat by ID. Used by CatSlot to change the selected cat.
    /// </summary>
    /// <param name="id"></param>
    public void setSelectedCat(int id)
    {
        GameObject cat = catObjs[id];
        if (cat != null)
        {
            selectedCatData = cat.GetComponent<CatInstance>().getCatData();
        }
        else
        {
            Debug.Log("Selected cat not found.");
        }
        // Reload InfoPanel
        UpdateCatInfoPanel();
    }

    /// <summary>
    /// Return the GameObject of the selected Cat
    /// </summary>
    /// <returns></returns>
    public GameObject getSelectedCat()
    {
        if (selectedCatData != null)
        {
            int id = selectedCatData.IdNum;
            return catObjs[id];
        }
        return null;
    }

    /// <summary>
    /// Return the CatData of the selected Cat
    /// </summary>
    /// <returns></returns>
    public CatData getSelectedCatData()
    {
        return selectedCatData;
    }

    /// <summary>
    /// Return the ID of the selected Cat
    /// </summary>
    /// <returns></returns>
    public int getSelectedCatID()
    {
        if (selectedCatData != null)
        {
            return selectedCatData.IdNum;
        }
        else return -1;
    }

    /// <summary>
    /// Debug method to test CatData list
    /// </summary>
    private void PrintCatList()
    {
        string catstring = "";
        foreach (CatData cat in cats)
        {
            catstring += cat.IdNum + ", ";
        }
        print(catstring);
    }

    /// <summary>
    /// Debug method to test Dictionary
    /// </summary>
    /// <returns></returns>
    private string CatDictionaryToString()
    {
        string catstring = "";
        List<int> ids = Enumerable.ToList(catObjs.Keys);
        foreach (int n in ids)
        {
            catstring += n + ", ";
        }
        return catstring;
    }

    /// <summary>
    /// Get list of CatData
    /// </summary>
    /// <returns></returns>
    public List<CatData> GetCats()
    {
        return cats;
    }

    /// <summary>
    /// Add set number of points to player points
    /// </summary>
    /// <param name="points"></param>
    public void AddPlayerPoints(int points)
    {
        playerPoints += points;
    }

    /// <summary>
    /// Check if player has enough points to buy the attribute before adding
    /// </summary>
    /// <param name="name"></param>
    /// <param name="value"></param>
    public bool BuyAttribute(string name)
    {
        //TODO add individual values later
        int value = 1;
        //if (markings.ContainsKey(name))
        //{
        //    value = 200;
        //}
        //else
        //{
        //    value = 400;
        //}

        if (playerPoints > value)
        {
            AddAttributeToCurrentCat(name);
            playerPoints -= value;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Add a marking or trait to an existing cat
    /// </summary>
    /// <param name="s"></param>
    private void AddAttributeToCurrentCat(string s)
    {
        CatInstance inst = catObjs[selectedCatData.IdNum].GetComponent<CatInstance>();
        string[] attrs;
        float[,] clrs;

        if (markings.ContainsKey(s))
        {
            //get markings list
            attrs = selectedCatData.GetMarkingKeys();
            clrs = selectedCatData.GetMarkingColors();
        }
        else //(traits.Contains(s))
        {
            //get traits list
            attrs = selectedCatData.GetTraitKeys();
            clrs = selectedCatData.GetTraitColors();
        }

        // -- Copy Data Arrays --
        //create new lists
        string[] nattr = new string[attrs.Length + 1];
        attrs.CopyTo(nattr, 0); // Copy old array
        float[,] nclrs = ExpandColorArray(clrs);

        //add new mark and color
        nattr[attrs.Length] = s;
        nclrs[attrs.Length, 0] = UnityEngine.Random.value;
        nclrs[attrs.Length, 1] = UnityEngine.Random.value;
        nclrs[attrs.Length, 2] = UnityEngine.Random.value;


        if (markings.ContainsKey(s))
        {
            //set markings and color in catdata
            selectedCatData.SetMarkingsList(nattr, nclrs);
        }
        else //traits.Contains(s))
        {
            //set markings and color in catdata
            selectedCatData.SetTraitList(nattr, nclrs);
        }

        //set instance
        inst.setCatData(selectedCatData);
    }

    /// <summary>
    /// Expand the color array by one
    /// </summary>
    /// <param name="arrIn"></param>
    /// <returns></returns>
    private float[,] ExpandColorArray(float[,] arrIn)
    {
        int newSize = arrIn.GetLength(0) + 1;
        float[,] exp = new float[newSize, 3];
        for (int i = 0; i < arrIn.GetLength(0); i++)
        {
            exp[i, 0] = arrIn[i, 0];
            exp[i, 1] = arrIn[i, 1];
            exp[i, 2] = arrIn[i, 2];
        }
        return exp;
    }

    /// <summary>
    /// Unfinished
    /// </summary>
    public void Save()
    {
        Debug.Log("Save Start");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        if (!File.Exists(Application.persistentDataPath + "/AstralCats.dat"))
        {
            file = File.Create(Application.persistentDataPath + "/AstralCats.dat");
        }
        else
        {
            file = File.Open(Application.persistentDataPath + "/AstralCats.dat", FileMode.Open);
        }

        SaveData data = new SaveData();
        // assign vars
        data.cats = new List<CatData>(cats);
        data.idNumPos = numCats;
        data.points = playerPoints;
        data.markingChecklist = markingChecklist.Values.ToArray<bool>();
        data.traitChecklist = traitChecklist.Values.ToArray<bool>();

        // serialize
        bf.Serialize(file, data);
        Debug.Log("Save End");
        file.Close();
    }

    /// <summary>
    /// Unfinished
    /// </summary>
    public void Load()
    {
        Debug.Log("Load Start");
        if (File.Exists(Application.persistentDataPath + "/AstralCats.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/AstralCats.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            cats = data.cats;
            numCats = data.idNumPos;
            playerPoints = data.points;
            // Get Check Data
            bool[] mcheck = data.markingChecklist;
            bool[] tcheck = data.traitChecklist;
            // Setup Check Dictionaries
            int i = 0;
            foreach (string s in markings.Keys)
            {
                markingChecklist[s] = mcheck[i];
                i++;
            }
            i = 0;
            foreach (string s in traits)
            {
                traitChecklist[s] = tcheck[i];
                i++;
            }

            file.Close();
        }
        else Debug.Log("File Not Found");

        // Reload Cats
        ReinstantiateCats();
        Debug.Log("Load End");
    }

    /// <summary>
    /// AutoSave when the application loses focus (is closed on ios or minimized)
    /// </summary>
    /// <param name="focus"></param>
    private void OnApplicationFocus(bool focus)
    {
        if(focus == false)
        {
            Save();
        }
    }
}

[Serializable]
class SaveData
{
    public int idNumPos;
    public int points;
    public List<CatData> cats;
    [OptionalField]
    public bool[] markingChecklist;
    [OptionalField]
    public bool[] traitChecklist;
}

[Serializable]
public class Marking
{
    public Material material;
    
    public string GetName()
    {
        return material.name;
    }
}