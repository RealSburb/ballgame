﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectObject_Script : MonoBehaviour {
    private CatData data;
    GameController_Script gc;

    // Use this for initialization
    void Start () {
        gc = GameController_Script.control;
        CatInstance inst = this.GetComponentInParent<CatInstance>();
        data = inst.getCatData();
	}

    // When this object is clicked on
    void OnMouseDown()
    {
        // Check if the mouse was clicked over a UI element
        if (!IsPointerOverUIObject()) //TODO this does NOT work with touches
        {
        // if a cat is currently selected do not select. Use InfoPanel to Deselect
        gc = GameController_Script.control;
        if (gc.getSelectedCat() == null)
            {
                setThisCatSelected();
                CatInfoPanel.catInfoPanel.ActivateInfoPanel();
            }
       }
    }

    /// <summary>
    /// Detects if tap is over GUI. Burrowed from: https://answers.unity.com/questions/1115464/ispointerovergameobject-not-working-with-touch-inp.html
    /// </summary>
    /// <returns></returns>
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }


    /* // TODO use raycasting to select a cat instead
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; // where the hit is stored
        

        if (Physics.Raycast(ray.origin, ray.direction, out hit, 1000, layermask)) // gets the hit and returns true or false
        {
            targetPosition = hit.point; // This is good?

            //this.transform.LookAt(targetPosition); // target should be good
            // calculate angle
            lookAtTarget = new Vector3(
                targetPosition.x - transform.position.x,
                transform.position.y,
                targetPosition.z - transform.position.z); // left and right differences in position
            playerRot = Quaternion.LookRotation(lookAtTarget); // angle to turn to look at positon
            moving = true;
        }
        */

    private void setThisCatSelected() {
        gc.setSelectedCat(gameObject);
        print("selected cat is " + data.IdNum);
    }
}
