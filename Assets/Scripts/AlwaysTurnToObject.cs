﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysTurnToObject : MonoBehaviour
{
    public Transform target;
    Vector3 targetPoint;
    Quaternion targetRotation;


    private void Update()
    {
        targetPoint = new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position;
        targetRotation = Quaternion.LookRotation(targetPoint, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);
    }
}
