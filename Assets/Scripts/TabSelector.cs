﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// In charge of tab coloring.
/// Note: Each button makes calls to the appropriate TabFiller.
/// </summary>
public class TabSelector : MonoBehaviour //TabController
{
    public Color defaultTextColor;
    public Color defaultBGColor;
    public Color greyTextColor;
    public Color greyBGColor;
    public CatInfoPanel infoPanel;

    //public GameObject[] screens;

    private void Awake()
    {
        SelectTab(transform.GetChild(0).gameObject);
    }

    /// <summary>
    /// When a tab is selected change all tabs to correct color and then load the correct data in the tab
    /// </summary>
    /// <param name="selected"></param>
    public void SelectTab(GameObject selected)
    {
        // for each button
        foreach(Transform child in transform)
        {      
            Text t = child.GetComponentInChildren<Text>();
            Button bg = child.GetComponent<Button>();

            // if this is not the selected button
            if (!selected.transform.Equals(child))
            {
                //Debug.Log(child.name+" NotSelected");
                // Set the color to the not-selected color
                t.color = defaultTextColor;
                ColorBlock cb = bg.colors;
                cb.normalColor = defaultBGColor;
                bg.colors = cb;
            }
            else
            {
                // Set the color to the selected color
                t.color = greyTextColor;
                ColorBlock cb = bg.colors;
                cb.normalColor = greyBGColor;
                bg.colors = cb;
                // Load the correct tab
                if(t.text.Equals("Cattributes"))
                    infoPanel.openTab(Tab.Cattributes);
                else if (t.text.Equals("Genetics"))
                    infoPanel.openTab(Tab.Genetics);
            }
        }
    }

}
