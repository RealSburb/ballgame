﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class BreedingController : MonoBehaviour {
    public int breedingCost;
    public CatInstance catLeft;
    public CatInstance catRight;
    public TabManager tabLeft;
    public TabManager tabRight;
    public Text nameLeft;
    public Text nameRight;

    private List<CatData> catsLeft = new List<CatData>();
    private List<CatData> catsRight = new List<CatData>();
    private List<CatData> cats;
    int leftIndex = 0, rightIndex = 0;
    float noise = 0.03f; // how much to deviate from max and min
    int adultAge = 100;

    // Use this for initialization
    void Start () {
        cats = GameController_Script.control.GetCats();
        splitMF();

        if(catsLeft.Count > 0)
        {
            LoadLeftCat(catsLeft[0]);
        }
        else
        {
            catLeft.gameObject.SetActive(false);
        }

        if (catsRight.Count > 0)
        {
            LoadRightCat(catsRight[0]);
        }
        else
        {
            catRight.gameObject.SetActive(false);
        }
    }

    private void splitMF()
    {
        foreach(CatData cat in cats)
        {
            if(cat.IsAdult())
            {
                if(cat.Gender == 0)
                {
                    catsLeft.Add(cat);
                }
                else
                {
                    catsRight.Add(cat);
                }
            }
        }
    }

    /// <summary>
    /// Set Gui of Left Cat
    /// </summary>
    private void LoadLeftCat(CatData data)
    {
        catLeft.setCatData(data);
        tabLeft.LoadGeneticsOfCat(data);
        nameLeft.text = data.Name;
    }

    /// <summary>
    /// Set Gui of Right Cat
    /// </summary>
    private void LoadRightCat(CatData data)
    {
        catRight.setCatData(data);
        tabRight.LoadGeneticsOfCat(data);
        nameRight.text = data.Name;
    }

    public void catUp(bool left)
    {
        if (left)
        {
            if (leftIndex+1 >= catsLeft.Count)
            {
                leftIndex = -1;
            }
            LoadLeftCat(catsLeft[++leftIndex]);
        }
        else
        {
            if (rightIndex + 1 >= catsRight.Count)
            {
                rightIndex = -1;
            }
            LoadRightCat(catsRight[++rightIndex]);
        }
    }
    public void catDown(bool left)
    {
        if (left && catsLeft.Count > 0)
        {
            if (leftIndex - 1 < 0)
            {
                leftIndex = catsLeft.Count;
            }
            LoadLeftCat((catsLeft[--leftIndex]));
        }
        else if (!left && catsRight.Count > 0)
        {
            if (rightIndex - 1 < 0)
            {
                rightIndex = catsRight.Count;
            }
            LoadRightCat(catsRight[--rightIndex]);
        }
    }

    public void breedCurrentCats()
    {
        if (catsLeft.Count == 0 || catsRight.Count == 0)
        {
            return;
        }
        // get current two cats
        CatData mother = catsLeft[leftIndex];
        CatData father = catsRight[rightIndex];
        // get playerpoints
        int points = GameController_Script.control.getPlayerPoints();
        if (points >= breedingCost)
        {
            // remove points
            GameController_Script.control.addPlayerPoints(-breedingCost);

            // New cat
            CatData kitten = new CatData(GameController_Script.control.GetAndIncrementID());
            kitten.Name = "Kitten";
            // Gender
            kitten.Gender = UnityEngine.Random.Range(0,2);
            // Base color
            kitten.SetBaseColor(PickChildColor(mother.GetBaseColorFloats(), father.GetBaseColorFloats()));
            // Eyes
            kitten.SetEyeColor(PickChildColor(mother.GetEyeColorFloats(), father.GetEyeColorFloats()));
            // Size
            kitten.Size = PickChildSize(mother.Size, father.Size);
            // BlendShapes
            kitten.SetBlendValues(PickBlendValues(mother.GetBlendValues(), father.GetBlendValues()));
            // Markings
            GenerateMarkings(kitten,mother,father);
            // Physical
            GenerateTraits(kitten, mother, father);
            // Stats
            GenerateStats(kitten, mother, father);

            // add cat
            GameController_Script.control.addKitten(kitten);
            SceneManager.LoadScene("Main");
        }
        else
        {
            Debug.Log("Kitten cannot be afforded");
            //TODO popup saying cannot afford
        }
    }

    private Dictionary<String, float[]> ArrayToDict(String[] marks, float[,] col)
    {
        Dictionary<String, float[]> dict = new Dictionary<String, float[]>();
        for (int i = 0; i < marks.Length; i++)
        {
            float[] cols = new float[3];
            for(int j = 0; j < 3; j++)
            {
                cols[j] = col[i, j];
            }
            dict[marks[i]] = cols;
        }
        return dict;
    }

    /// <summary>
    /// Generates the child's markings. 100% chance of a marking carried by both parents. 30% if it is carried by only 1.
    /// </summary>
    /// <param name="kitten"></param>
    /// <param name="mother"></param>
    /// <param name="father"></param>
    private void GenerateMarkings(CatData kitten, CatData mother, CatData father)
    {
        Dictionary<String, float[]> finalMarks = new Dictionary<string, float[]>();

        // Get all markings and colors
        Dictionary<String, float[]> mdict = ArrayToDict(mother.GetMarkingKeys(), mother.GetMarkingColors());
        Dictionary<String, float[]> fdict = ArrayToDict(father.GetMarkingKeys(), father.GetMarkingColors());

        // Pick markings
        // for each marking on the mother's side check if the father has it too
        foreach (String mark in mdict.Keys)
        {
            //check if father also has marking
            if (fdict.ContainsKey(mark))
            {
                // if certain can pick a color now
                float[] mcol = mdict[mark];
                float[] fcol = fdict[mark];
                float[] childColor = PickChildColor(mcol, fcol);
                // put mark and color
                finalMarks[mark] = childColor;
                // remove from father (already taken care of)
                fdict.Remove(mark);
            }
            else
            {
                // 30% chance to pass it on
                if(UnityEngine.Random.value <= 0.3f)
                {
                    finalMarks[mark] = mdict[mark];
                }
            }

        }
        foreach (String mark in fdict.Keys)
        {
            // 40% chance to pass it on
            if (UnityEngine.Random.value <= 0.4f)
            {
                finalMarks[mark] = fdict[mark];
            }
        }

        string[] keys = Enumerable.ToArray<string>(finalMarks.Keys);
        float[,] colors = new float[keys.Length,3];
        // get float array of rgb for each key
        for (int i = 0; i < keys.Length;i++)
        {
            float[] newcols = finalMarks[keys[i]];
            colors[i, 0] = newcols[0];
            colors[i, 1] = newcols[1];
            colors[i, 2] = newcols[2];
        }
        // Assign markings
        kitten.SetMarkingsList(keys, colors);
    }

    private void GenerateTraits(CatData kitten, CatData mother, CatData father)
    {
        Dictionary<String, float[]> finalTraits = new Dictionary<string, float[]>();

        // Get all markings and colors
        Dictionary<String, float[]> mdict = ArrayToDict(mother.GetTraitKeys(), mother.GetTraitColors());
        Dictionary<String, float[]> fdict = ArrayToDict(father.GetTraitKeys(), father.GetTraitColors());


        // Pick markings
        // for each marking on the mother's side check if the father has it too
        foreach (String trait in mdict.Keys)
        {
            //Debug.Log("MTrait: " + trait);
            //check if father also has marking
            if (fdict.ContainsKey(trait))
            {
                //Debug.Log("Father also contains: " + trait);
                // if certain can pick a color now
                float[] mcol = mdict[trait];
                float[] fcol = fdict[trait];
                float[] childColor = PickChildColor(mcol, fcol);
                // put mark and color
                finalTraits[trait] = childColor;
                // remove from father (already taken care of)
                fdict.Remove(trait);
            }
            else
            {
                //Debug.Log("FatherDoes not contain: " + trait);
                // 40% chance to pass it on
                if (UnityEngine.Random.value <= 0.4f)
                {
                    finalTraits[trait] = mdict[trait];
                }
            }

        }
        foreach (String trait in fdict.Keys)
        {
            //Debug.Log("FTrait: " + trait);
            // 40% chance to pass it on
            if (UnityEngine.Random.value <= 0.4f)
            {
                finalTraits[trait] = fdict[trait];
            }
        }

        string[] keys = Enumerable.ToArray<string>(finalTraits.Keys);
        float[,] colors = new float[keys.Length, 3];
        // get float array of rgb for each key
        for (int i = 0; i < keys.Length; i++)
        {
            float[] newcols = finalTraits[keys[i]];
            colors[i, 0] = newcols[0];
            colors[i, 1] = newcols[1];
            colors[i, 2] = newcols[2];
        }
        // Assign markings
        kitten.SetTraitList(keys, colors);
    }



    private float PickChildSize(float mother, float father)
    {
        float noise = 0.2f;
        float initSize = UnityEngine.Random.Range(Math.Min(mother, father) - noise, Math.Max(mother, father) + noise);
        if(initSize > 10)
        {
            return 10;
        }
        if(initSize < 0)
        {
            return 0;
        }
        return initSize;
    }

    /// <summary>
    /// Chose a color between the mother and father's colors with noise.
    /// </summary>
    /// <param name="mother"></param>
    /// <param name="father"></param>
    /// <returns></returns>
    private float[] PickChildColor(float[] mother, float[] father)
    {
        float[] kc = new float[3];
        int lean = UnityEngine.Random.Range(0, 3);
        switch (lean)
        {
            case 0: // Mother with noise
                Debug.Log("mother");
                kc[0] = UnityEngine.Random.Range(mother[0] - noise, mother[0] + noise);
                kc[1] = UnityEngine.Random.Range(mother[1] - noise, mother[1] + noise);
                kc[2] = UnityEngine.Random.Range(mother[2] - noise, mother[2] + noise);
                break;
            case 1: // Father with noise
                Debug.Log("father");
                kc[0] = UnityEngine.Random.Range(father[0] - noise, father[0] + noise);
                kc[1] = UnityEngine.Random.Range(father[1] - noise, father[1] + noise);
                kc[2] = UnityEngine.Random.Range(father[2] - noise, father[2] + noise);
                break;
            default: // Blend
                Debug.Log("blend");
                kc[0] = UnityEngine.Random.Range(Math.Min(mother[0], father[0]), Math.Max(mother[0], father[0]));
                kc[1] = UnityEngine.Random.Range(Math.Min(mother[1], father[1]), Math.Max(mother[1], father[1]));
                kc[2] = UnityEngine.Random.Range(Math.Min(mother[2], father[2]), Math.Max(mother[2], father[2]));
                break;
        }
        // clamp
        for(int i = 0; i < 3; i++)
        {
            if(kc[i] > 1)
            {
                kc[i] = 1;
            }
            if (kc[i] < 0)
            {
                kc[i] = 0;
            }
        }
        return kc;
    }

    private float[] PickBlendValues(float[] mother, float[] father)
    {
        float[] blendValues = new float[mother.Length];
        for(int i=0; i < mother.Length; i++)
        {
            blendValues[i] = UnityEngine.Random.Range(Math.Min(mother[i], father[i])-noise, Math.Max(mother[i], father[i])+noise);
            // clamp
            if (blendValues[i] > 100)
            {
                blendValues[i] = 100;
            }
            if (blendValues[i] < 0)
            {
                blendValues[i] = 0;
            }
        }
        return blendValues;
    }

    private void GenerateStats(CatData kitten, CatData mother, CatData father) //TODO
    {
        // bool incr = UnityEngine.Random.Range(0, 31) == 0;
        // bool decr = UnityEngine.Random.Range(0, 31) == 0;
        // float noise = 1;
        // float atk = UnityEngine.Random.Range(Math.Min(mother.GetBaseAtk(), father.GetBaseAtk()) - noise, Math.Max(mother.GetBaseAtk(), father.GetBaseAtk()) + noise);
        kitten.SetBaseAtk(UnityEngine.Random.Range(1, 6));
        kitten.SetBaseStl(UnityEngine.Random.Range(1, 6));
        kitten.SetBaseHp(UnityEngine.Random.Range(1, 6));
        kitten.SetBaseAcc(UnityEngine.Random.Range(1, 6));
    }
}
