﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuideBook : MonoBehaviour
{
    public Transform contentPane;
    public Transform toggleFab;
    private List<ToggleFab> toggles;

    void Start()
    {
        toggles = new List<ToggleFab>();
        Dictionary<string,bool> markings = GameController_Script.control.GetMarkingChecklist();
        Dictionary<string, bool> traits = GameController_Script.control.GetTraitChecklist();

        int i = 0;
        foreach(string s in markings.Keys)
        {
            createToggleFab(s, markings[s]);
            i++;
        }
        i = 0;
        foreach (string s in traits.Keys)
        {
            createToggleFab(s, traits[s]);
            i++;
        }
    }

    /// <summary>
    /// Create a Toggle Prefab from a name and check value
    /// </summary>
    /// <param name="s"></param>
    /// <param name="check"></param>
    private void createToggleFab(string s, bool check)
    {
        // Instantiate Panel
        GameObject panel = Instantiate(toggleFab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
        // Parent Panel
        panel.transform.SetParent(contentPane, false);

        // Setup Toggle
        ToggleFab tf = panel.GetComponent<ToggleFab>();
        tf.SetText(s);
        tf.Toggle(check);
        toggles.Add(tf);
        Debug.Log("Added toggle");
    }

    public void UpdateChecks()
    {
        if(toggles == null)
        {
            Debug.Log("Toggles are null");
            return;
        }
        Dictionary<string, bool> markings = GameController_Script.control.GetMarkingChecklist();
        Dictionary<string, bool> traits = GameController_Script.control.GetTraitChecklist();
        int i = 0;
        foreach(string mark in markings.Keys)
        {
            if(toggles[i].isChecked() != markings[mark]) // if Guidebook has a different status then change the toggle
            {
                toggles[i].Toggle(markings[mark]);
            }
            i++;
        }
        i = 0;
        foreach (string trait in traits.Keys)
        {
            if (toggles[i].isChecked() != traits[trait]) // if Guidebook has a different status then change the toggle
            {
                toggles[i].Toggle(traits[trait]);
            }
            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
