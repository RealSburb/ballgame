﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddRemCats_Script : MonoBehaviour {
    int costToAddCat = 20;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addCat()
    {
        if(GameController_Script.control.getPlayerPoints() >= costToAddCat)
        {
            GameController_Script.control.AddPlayerPoints(-costToAddCat);
            GameController_Script.control.AddNewCatToScene();
        }
    }

    //public void removeCat(CatPanel_Script panel)
    //{
      //  GameController_Script.control.removeCat(panel);
    //}
}
