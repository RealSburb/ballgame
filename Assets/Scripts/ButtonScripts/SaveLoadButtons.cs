﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadButtons : MonoBehaviour
{
    public void save()
    {
        GameController_Script.control.Save();
    }

    public void load()
    {
        GameController_Script.control.Load();
    }
}
