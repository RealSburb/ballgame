﻿using UnityEngine;

public class ActivateDeactivate_Script : MonoBehaviour {

    public void ActivatePanel(GameObject panel)
    {
        panel.SetActive(true);
    }
    public void DeactivatePanel(GameObject panel)
    {
        panel.SetActive(false);
    }
    public void TogglePanel(GameObject panel)
    {
        if (panel.activeSelf)
        {
            panel.SetActive(false);
        }
        else
        {
            panel.SetActive(true);
        }
    }
}
