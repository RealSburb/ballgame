﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureButton : MonoBehaviour
{
    public static GameObject adventureButton;

    // Start is called before the first frame update
    void Start()
    {
        adventureButton = gameObject;
        gameObject.SetActive(false);
    }

}
