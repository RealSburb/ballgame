﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Delete self on tap and notify explore. Only for use on the Explore Scene
/// </summary>
public class Pickup : MonoBehaviour
{
    private string key;
    public SpriteRenderer sprite;

    private Transform target;
    public Vector3 velocity = Vector3.zero;
    public float modifier;
    private bool canDestroy;

    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cat"))
        {
            ExploreControl.explore.CheckQuests(key);
            Destroy(gameObject);
        }
    }*/

    /*public void OnMouseDown()
    {
        ExploreControl.explore.CheckQuests(key);
        Destroy(gameObject);
    }*/

    /// <summary>
    /// After canDestroy destroys on entry
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Cat" && canDestroy)
        {
            CollectPickup();
        }
    }

    /// <summary>
    /// After canDestroy destroys if it stays colliding
    /// </summary>
    /// <param name="other"></param>
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Cat" && canDestroy)
        {
            CollectPickup();
        }
    }

    private void CollectPickup()
    {
        ExploreControl.explore.CheckQuests(key);
        Destroy(gameObject);
    }

    /// <summary>
    /// Set sprite and name of pickup. Name is used to match the item to quests.
    /// </summary>
    /// <param name="pickupName"></param>
    /// <param name="s"></param>
    public void buildPickup(string pickupName,Sprite s)
    {
        key = pickupName;
        sprite.sprite = s;
    }

    private void Start()
    {
        target = GameController_Script.control.getSelectedCat().transform;
        StartCoroutine(WaitToFly());
    }

    // Update is called once per frame
    void Update()
    {
        if (canDestroy) /// If the time has elapsed before Collecting the item move to cat
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, Time.deltaTime * modifier);
        }
    }

    /// <summary>
    /// Waits a set time before flying back to the player
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitToFly()
    {
        yield return new WaitForSeconds(1.5f);
        canDestroy = true;
        /// If pickup hasn't collided after 2 seconds remove it
        yield return new WaitForSeconds(2f);
        CollectPickup();
    }
}
