﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrades : MonoBehaviour
{
    public Transform upFab;
    public Transform contentPane;
    public int markingCost = 100;
    //public int traitCost;

    // Start is called before the first frame update
    void Start()
    {
        UpdateList();
    }

    /// <summary>
    /// Get list of Availible Attributes and display them
    /// </summary>
    public void UpdateList()
    {
        // Clear old
        foreach (Transform child in contentPane.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        Dictionary<string,bool> marks = GameController_Script.control.GetMarkingChecklist();
        Dictionary<string, bool> traits = GameController_Script.control.GetTraitChecklist();
        foreach(string s in marks.Keys)
        {
            
            if(marks[s] == true)
            {
                //check if cat already has marking
                CatData sc = GameController_Script.control.getSelectedCatData();
                if (!Array.Exists(sc.GetMarkingKeys(), element => element.Equals(s)) && !Array.Exists(sc.GetTraitKeys(), element => element.Equals(s)))
                {
                    // Instantiate Panel
                    GameObject panel = Instantiate(upFab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
                    // Parent Panel
                    panel.transform.SetParent(contentPane, false);

                    // Setup Toggle
                    UpgradesFab tf = panel.GetComponent<UpgradesFab>();
                    tf.SetText(s, markingCost);
                }
            }
        }
    }

    /// <summary>
    /// remove bought fab
    /// </summary>
    /// <param name="fab"></param>
    public void RemoveFab(GameObject fab)
    {
        Destroy(fab);
    }

}
