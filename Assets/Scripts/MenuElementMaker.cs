﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuElementMaker
{
    private Font font;
    private Transform t;

    /// <summary>
    /// Instantiate the font and the game object to attach objects to
    /// </summary>
    /// <param name="f"></param>
    /// <param name="h"></param>
    public MenuElementMaker(Transform tin)
    {
        font = GameController_Script.control.font;
        t = tin;
    }

    /// <summary>
    /// Create a text object and add it
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public GameObject createAndAddTextObj(string text)
    {
        GameObject textHolder = createTextObj(text);
        // Parent
        textHolder.transform.SetParent(t, false);
        return textHolder;
    }

    /// <summary>
    /// Create a text element and return it
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public GameObject createTextObj(string text)
    {
        GameObject textHolder = new GameObject();
        textHolder.AddComponent<Text>();
        Text textComp = textHolder.GetComponent<Text>();
        textComp.text = text;
        textComp.font = font;
        textComp.color = Color.black;
        textComp.fontSize = 18; //textComp.fontSize = 14;
        //textComp.fontStyle = FontStyle.Bold;
        return textHolder;
    }

    /// <summary>
    /// Create a Text Element with a Background Color and add it
    /// </summary>
    /// <param name="text"></param>
    /// <param name="c"></param>
    /// <returns></returns>
    public GameObject createAndAddTextObjWithBGColor(string text, Color c)
    {
        GameObject textHolder = createTextObj(text);

        GameObject bg = new GameObject();
        bg.AddComponent<Image>().color = c;

        textHolder.AddComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textHolder.transform.SetParent(bg.transform);
        bg.transform.SetParent(t, false);
        return bg;
    }

}
