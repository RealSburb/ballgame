﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatRespawner : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.HasKey("FirstLoad"))
        {
            GameController_Script.control.ReinstantiateCats();
        }
        else
        {
            PlayerPrefs.SetInt("FirstLoad", 1);
        }
	}
}
