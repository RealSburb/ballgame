﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesFab : MonoBehaviour
{
    public Text text;
    public Text value;

    /// <summary>
    /// Call this on click
    /// </summary>
    public void BuyMarking()
    {
        if (GameController_Script.control.BuyAttribute(text.text))
        {
            Destroy(gameObject); // Destroy self on success
        }
    }

    /// <summary>
    /// Set marking name
    /// </summary>
    /// <param name="s"></param>
    public void SetText(string s, int val)
    {
        text.text = s;
        value.text = val+"";
    }
}
