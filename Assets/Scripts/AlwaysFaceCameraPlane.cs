﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysFaceCameraPlane : MonoBehaviour {
    public bool flip = false;

	// Use this for initialization
	void Start () {
		
	}

    /// <summary>
    /// Face so that the front is completely visible
    /// </summary>
    private void LateUpdate()
    {
        Debug.DrawRay(transform.position, Camera.main.transform.forward,Color.green);
        if (flip)
        {
            transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward);
        }
    }
}
