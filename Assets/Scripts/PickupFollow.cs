﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pickups fly to selected cat and destroy themselves after a set time
/// </summary>
public class PickupFollow : MonoBehaviour
{
    //TODO make collider a trigger
    private Transform target;
    public Vector3 velocity = Vector3.zero;
    public float modifier;
    private bool canDestroy;

    // Start is called before the first frame update
    void Start()
    {
        target = GameController_Script.control.getSelectedCat().transform;
        StartCoroutine(WaitToFly());
    }

    // Update is called once per frame
    void Update()
    {
        if (canDestroy)
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, Time.deltaTime * modifier);
        }
    }

    IEnumerator WaitToFly()
    {
        yield return new WaitForSeconds(0.5f);
        canDestroy = true;
    }

    /// <summary>
    /// Needs to wait before destroy
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Cat" && canDestroy)
        {
            //ExploreControl.explore.CheckQuests(key);
            Destroy(gameObject);
        }
    }
}
