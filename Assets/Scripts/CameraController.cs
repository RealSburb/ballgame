﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Modes the camera can be in
/// </summary>
public enum CameraMode { Landscape, LeftOrient, MiddleOrient, RightOrient, Adventure };

/// <summary>
/// WIP class Controls the camera on the main screen
/// </summary>
public class CameraController : MonoBehaviour
{
    private Camera cam;
    int SmoothFactor = 6;

    private Vector3 defaultPos;
    private Vector3 defaultRot;
    private float defaultZoom;

    private CameraMode cameraMode = CameraMode.Landscape;

    // Start is called before the first frame update
    void Start()
    {
        defaultPos = transform.position;
        defaultRot = transform.eulerAngles;
        cam = GetComponent<Camera>();
        defaultZoom = cam.fieldOfView;
    }

    // Update is called once per frame
    void Update()
    {
        if (cameraMode == CameraMode.Landscape)
        {
            // Go to default position and rotation
            transform.position = Vector3.Slerp(transform.position, defaultPos, SmoothFactor * Time.deltaTime);
            //transform.eulerAngles = defaultRot; // TODO
            //cam.fieldOfView = defaultZoom; // Field of View Modified relative to cat size when selected
        }
        else if(cameraMode == CameraMode.LeftOrient) // Right panel Open
        {
            // Modify position + Offset

        }
        else if (cameraMode == CameraMode.RightOrient) // Left panel open
        {
            // Modify position - Offset
        }
        else if (cameraMode == CameraMode.MiddleOrient) // both or no panels open
        {
            // Align camera with cat. No offset
        }

    }

    public void setCameraMode(CameraMode mode)
    {
        this.cameraMode = mode;
    }

}
