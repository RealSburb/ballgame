﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour
{
    // Wander
    private float wanderTime;
    public float movementSpeed = 0.0005f;
    private Quaternion targetRotation;

    public void setSpeed(float speed)
    {
        movementSpeed = speed;
    }

    private void Start()
    {
        StartCoroutine(ChangeWanderTarget());
    }

    private void FixedUpdate()
    {
        //
        LookAhead();
        // Move forwards
        Vector3 m = Vector3.forward * (movementSpeed);
        transform.Translate(m);

        if (!transform.rotation.Equals(targetRotation))
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 4 * movementSpeed);
        }
    }


    private void LookAhead()
    {
        float rayMagnitude = 10;
        Vector3 moveTo = transform.position + (transform.forward*rayMagnitude);
        //Debug.DrawLine(transform.position, moveTo, Color.green, 0.5f); // shows twice as long as it actually is

        Ray ray = new Ray(transform.position,moveTo);
        RaycastHit hit; // where the hit is stored

        if (Physics.Raycast(ray.origin, ray.direction, out hit, rayMagnitude) && !hit.transform.gameObject.CompareTag("Ground") && !hit.transform.gameObject.CompareTag("Prey") && !hit.transform.gameObject.CompareTag("Monster"))
        {
                // Move away
                Vector3 pos = transform.position;
                pos = new Vector3(-pos.x, 0, -pos.z);
                targetRotation = Quaternion.LookRotation(pos);
                // speed of the turn should be dependant on how close the collision is
        }
    }

    // ------------------ WANDER ----------------------- //
    IEnumerator ChangeWanderTarget()
    {
        float wanderTime = Random.Range(5.0f, 15.0f);
        ChooseRotation();
        yield return new WaitForSeconds(wanderTime);
        StartCoroutine(ChangeWanderTarget());
    }

    /// <summary>
    /// Chose a new target to wander towards
    /// </summary>
    private void ChooseRotation()
    {
        // find a position at a random degrees away
        float angle = Random.Range(0, 360) - 180; // rotate from -180 to 180 across y
        // get current rotation
        Quaternion newRotation = transform.rotation;
        // multiply by the degree turn
        newRotation *= Quaternion.Euler(0, angle, 0);
        targetRotation = newRotation;
        //Debug.Log("NewRot: " + angle);
    }

    // ------------------ HUNT ----------------------- //
}