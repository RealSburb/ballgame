﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy instance
/// </summary>
public class Enemy : RPGAttributes
{

    public RPGAttributes GetAttributes()
    {
        return this;
    }

    /// <summary>
    /// Generate a level from a tier
    /// </summary>
    /// <param name="type"></param>
    /// <param name="tier"></param>
    public Enemy(EnemyType type, EnemyTier tier)
    {
        // Set name
        this.name = type.name;

        // Generate Level
        switch (tier)
        {
            case EnemyTier.Beginner:
                level = UnityEngine.Random.Range(0, 6);
                break;
            case EnemyTier.Novice:
                level = UnityEngine.Random.Range(6, 11);
                break;
            case EnemyTier.Intermediate:
                level = UnityEngine.Random.Range(11, 21);
                break;
            case EnemyTier.Advanced:
                level = UnityEngine.Random.Range(21, 31);
                break;
            case EnemyTier.Experienced:
                level = UnityEngine.Random.Range(31, 51);
                break;
            case EnemyTier.Hard:
                level = UnityEngine.Random.Range(51, 81);
                break;
            case EnemyTier.Expert:
                level = UnityEngine.Random.Range(81, 101);
                break;
            default:
                Debug.Log("Tier " + tier + " not found");
                break;
        }

        // Generate Stats
        GenerateStats(level, type);

        // Set Hp
        remainingHp = hp;
    }

    /// <summary>
    /// Generate From a Cat's data
    /// </summary>
    /// <param name="type"></param>
    /// <param name="tier"></param>
    public Enemy(CatData c, EnemyTier tier)
    {
        name = "Wild Cat";
        // Generate Level
        switch (tier)
        {
            case EnemyTier.Beginner:
                level = UnityEngine.Random.Range(0, 6);
                break;
            case EnemyTier.Novice:
                level = UnityEngine.Random.Range(6, 11);
                break;
            case EnemyTier.Intermediate:
                level = UnityEngine.Random.Range(11, 21);
                break;
            case EnemyTier.Advanced:
                level = UnityEngine.Random.Range(21, 31);
                break;
            case EnemyTier.Experienced:
                level = UnityEngine.Random.Range(31, 51);
                break;
            case EnemyTier.Hard:
                level = UnityEngine.Random.Range(51, 81);
                break;
            case EnemyTier.Expert:
                level = UnityEngine.Random.Range(81, 101);
                break;
            default:
                Debug.Log("Tier " + tier + " not found");
                break;
        }

        GenerateRandomStats(level);

        // Set Hp
        remainingHp = hp;
    }

    /// <summary>
    /// Create Generic Lv1 enemy
    /// </summary>
    public Enemy()
    {
        GenerateRandomStats(1);
    }

    private void GenerateStats(int level, EnemyType et)
    {
        int points = level;
        Debug.Log("Points " + points);
        ArrayList leftoverStats = new ArrayList();

        // Give lv 0 a debuff
        if (level == 0)
        {
            hp = 5;
        }

        // find most dominant stat
        foreach (StatContainer sc in et.stats)
        {
            int ap = (int)(level * sc.percentStats * 0.01);
            switch (sc.stat)
            {
                case Stat.hp:
                    hp += 2 * ap;
                    //Debug.Log("Hp Points " + ap);
                    break;
                case Stat.accuracy:
                    accuracy += ap;
                    //Debug.Log("Acc Points " + ap);
                    break;
                case Stat.stealth:
                    stealth += ap;
                    //Debug.Log("Stealth Points " + ap);
                    break;
                case Stat.strength:
                    strength += ap;
                    // Debug.Log("Str Points " + ap);
                    break;
                default:
                    Debug.Log(sc.stat + " not a valid Stat");
                    break;
            }
            points -= ap;
            //Debug.Log("Points " + points);
        }
        Debug.Log("Enemy Leftover Points " + points);
        // leftovers
        for (int i = 0; i < points; i++)
        {
            int stat = UnityEngine.Random.Range(0, 4);
            switch ((Stat)stat)
            {
                case Stat.hp:
                    hp += 2;
                    Debug.Log("Hp " + hp);
                    break;
                case Stat.accuracy:
                    accuracy++;
                    Debug.Log("Acc " + accuracy);
                    break;
                case Stat.stealth:
                    stealth++;
                    Debug.Log("Stl " + stealth);
                    break;
                case Stat.strength:
                    strength++;
                    Debug.Log("Str " + strength);
                    break;
                default:
                    Debug.Log(stat + " not a valid Stat");
                    break;
            }
        }
    }

    /// <summary>
    /// Used to generate stats for a cat at a level
    /// </summary>
    /// <param name="level"></param>
    private void GenerateRandomStats(int level)
    {
        int points = level;

        for (int i = 0; i < points; i++)
        {
            int stat = UnityEngine.Random.Range(0, 4);
            switch ((Stat)stat)
            {
                case Stat.hp:
                    hp += 2;
                    Debug.Log("Hp " + hp);
                    break;
                case Stat.accuracy:
                    accuracy++;
                    Debug.Log("Acc " + accuracy);
                    break;
                case Stat.stealth:
                    stealth++;
                    Debug.Log("Stl " + stealth);
                    break;
                case Stat.strength:
                    strength++;
                    Debug.Log("Str " + strength);
                    break;
                default:
                    Debug.Log(stat + " not a valid Stat");
                    break;
            }
        }
    }

    /// <summary>
    /// Reduce damage by offending attack and return the remaining hp
    /// </summary>
    /// <param name="dmg"></param>
    /// <returns></returns>
    public int TakeDamage(int dmg)
    {
        //TODO account for element
        remainingHp = (remainingHp - dmg);
        return remainingHp;
    }

    /// <summary>
    /// Calculate the amount of experience that this enemy gives.s
    /// </summary>
    /// <returns></returns>

    public string GetName()
    {
        return name;
    }
    public int GetLevel()
    {
        return level;
    }
    public int GetHp()
    {
        return hp;
    }
    public int GetRemainingHP()
    {
        return remainingHp;
    }
    public int GetDamage()
    {
        return UnityEngine.Random.Range(accuracy, strength + 1); ;
    }

    public int GetStl() { return stealth; }
}

/// <summary>
/// Holds data for a type of enemy
/// Enter the percent of the total stat distribution that the enemy should have
/// </summary>
[Serializable]
public class EnemyType
{
    public string name;
    public EnemyTier tier; // range of levels this enemy can be
    public StatContainer[] stats = {
       new StatContainer(Stat.hp, 25),
       new StatContainer(Stat.strength, 25),
       new StatContainer(Stat.accuracy, 25),
       new StatContainer(Stat.stealth, 25)
        };
}

[Serializable]
public class StatContainer
{
    public Stat stat;
    public int percentStats;
    public StatContainer(Stat stat, int percentStats)
    {
        this.stat = stat;
        this.percentStats = percentStats;

    }
}

/// <summary>
/// Used to set stat points on cats.
/// </summary>
public enum Stat
{
    hp,
    strength,
    accuracy,
    stealth
}

/// <summary>
/// Used to assign values to both the enemy and cat stat panels
/// </summary>
[System.Serializable]
public class RPGAttributes
{
    public string name;
    public int level = 0;
    public int remainingHp = 6;
    public int hp = 6;
    public int strength = 1;
    public int accuracy = 0;
    public int stealth = 0;

    public Attack[] attacks = new Attack[] { new Attack() };
}

public enum EnemyTier
{
    Beginner = 0, // 0-5
    Novice = 1,  // 6-10
    Intermediate = 2, // 11-20
    Advanced = 3, //21-30
    Experienced = 4, // 31-50
    Hard = 5, //51-80
    Expert = 6 // 81-100
}