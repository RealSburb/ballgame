﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestController : MonoBehaviour
{
    //// GUI
    public RectTransform playerHpBar;
    public RectTransform expBar;
    public RectTransform enemyHpBar;
    public CatInstance catInstance;
    public GameObject enemyInstance;
    public GameObject attackContainer;
    public Transform attackButton;
    //public CatInstance enemyCat;
    
    //// Enemy
    private Enemy enemy;
    private CatInstance enemyCat;
    private bool inBattle = false;
    private bool enemyIsCat = false;

    ////
    private RPGAttributes player;
    private CatData playerData;

    public void Start()
    {
        // Load cat
        playerData = GameController_Script.control.getSelectedCatData();
        catInstance.setCatData(playerData);
        player = playerData.GetRPGAttr();
        // Load GUI
        AttachButtons();

        // Start First Fight
        StartFight();


            // When the attack is clicked pass the Attack to Player turn
            // PlayerTurn will calculate damage and apply it to the enemy and enemy bar

        // Set Hp bar for debug
        //SetBar(0.5f,playerHpBar);

    }

    private void AttachButtons()
    {
        foreach(Attack a in player.attacks)
        {
            // Create Button with AttackButton script
            // Set the Attack on the AttackButton
        }
    }

    public void PassAttack(GameObject go)
    {
        PlayerTurn(go.GetComponent<AbilityButton>().attack);
    }

    public void PlayerTurn(Attack atk)
    {
        Debug.Log("Starting Hp: " + enemy.remainingHp);
        //// Get Damage
        int baseDmg = player.strength + atk.DamageDealt();
        Debug.Log("Base Damage Dealt: " + baseDmg);
        // Reduce Enemy RemainingHP
        enemy.TakeDamage(baseDmg);
        Debug.Log("Remaining Hp: " + enemy.remainingHp);
        SetBar(((float)enemy.remainingHp/(float)enemy.hp), enemyHpBar);
        // Check if the battle is over
        IsBattleOver();
        // Enemy Turn
    }

    public void StartFight()
    {
        // Reset Bars
        SetBar(1f, playerHpBar);
        SetBar(1f, enemyHpBar);
        // Make Enemy
        GenerateEnemy();
    }

    /// <summary>
    /// Generate a Basic Enemy for the battle
    /// </summary>
    public void GenerateEnemy()
    {
        enemyIsCat = false;
        //enemy = GameController.Instance.GenerateEnemy(GameController.Instance.GetSelectedCat().data.level);
        enemy = new Enemy();
    }

    private void SetBar(float percent, RectTransform bar)
    {
        bar.localScale = new Vector3(percent, 1, 1);
    }

    public void EnemyTurn()
    {
        // Get Damage
        // Reduce Player RemainingHP
        // Re-enable player actions

    }

    private bool IsBattleOver()
    {
        if(player.remainingHp <= 0)
        {
            // Loss
            return true;
        }

        if (enemy.remainingHp <= 0)
        {
            // Win
            return true;
        }

        return false;
    }

    //public void RefreshCatPanel()
    //{
    //    // Set Exp bar
    //    SetExpBar(GameController.Instance.GetSelectedCat().GetExpPercent());
    //    // Set Health bar
    //    SetHealthBar(GameController.Instance.GetSelectedCat().GetHpPercent(), true);
    //    // Set Cat Attributes
    //    if (GameController.Instance.GetSelectedCat() != null)
    //    {
    //        playerPanel.SetAttributes(GameController.Instance.GetSelectedCat().GetCatAttr());
    //        playerPanel.SetPoints(GameController.Instance.GetSelectedCat().GetPoints());
    //    }
    //}

    //public void RefreshEnemyPanel()
    //{
    //    if (enemyIsCat)
    //    {
    //        enemyCatSprite.SetSprite(enemyCat);
    //        enemyCatSprite.gameObject.SetActive(true);
    //    }
    //    else
    //    {
    //        enemyCatSprite.gameObject.SetActive(false);
    //    }
    //    // Set Enemy health
    //    SetHealthBar((float)enemy.GetRemainingHP() / enemy.GetHp(), false);
    //    // Set Enemy Attributes
    //    enemyPanel.SetAttributes(enemy);
    //    // Turn Panel On
    //    enemyPanel.gameObject.SetActive(true);
    //}

    //public void NewBattle()
    //{
    //    RemoveWinText();

    //    if (enemy == null)
    //    {
    //        //Generate Enemy
    //        if (Random.Range(0, 100) < 10)
    //        {
    //            //Cat
    //            enemyIsCat = true;
    //            enemyCat = GameController.Instance.GenerateCat();
    //            enemy = GameController.Instance.GenerateEnemyCat(enemyCat);

    //        }
    //        else
    //        {
    //            //Prey or Monster
    //            enemyIsCat = false;
    //            enemy = GameController.Instance.GenerateEnemy(GameController.Instance.GetSelectedCat().data.level);
    //        }


    //        //TODO turn on enemy panel
    //        RefreshEnemyPanel();
    //    }

    //    //Restore Cat
    //    GameController.Instance.GetSelectedCat().RestoreHP();

    //    inBattle = true;
    //}

    //public void PlayerTurn()
    //{
    //    if (inBattle)
    //    {
    //        int cAtk = GameController.Instance.GetSelectedCat().GetDamage();
    //        int eHP = enemy.TakeDamage(cAtk, GameController.Instance.GetSelectedCat().GetStl());
    //        //AppendText("Player cat attacks for " + cAtk + ".");
    //        SetHealthBar((float)eHP / enemy.GetHp(), false);
    //        //AppendText("Enemy hp: " + eHP + "/" + enemy.GetHp());
    //        if (eHP <= 0)
    //        {
    //            GameController.Instance.GetSelectedCat().ApplyExp(enemy.level);
    //            Debug.Log("Enemy is Cat: " + enemyIsCat);
    //            if (enemyIsCat)
    //            {
    //                GameController.Instance.AddCat(enemyCat);
    //                SetWinText("Win: New Cat!");
    //            }
    //            else
    //            {
    //                SetWinText();
    //            }


    //            if (Random.Range(0, 1) < 1)
    //            {
    //                // TODO
    //                //AppendText("Picked up: " + DropItem().name);
    //            }

    //            Debug.Log(GameController.Instance.GetSelectedCat().CatToString());

    //            EndBattle();
    //        }
    //        else
    //        {
    //            EnemyTurn();
    //        }
    //    }

    //}


    //private void EndBattle()
    //{
    //    enemyPanel.gameObject.SetActive(false);
    //    inBattle = false;
    //    enemy = null;
    //    GameController.Instance.GetSelectedCat().RestoreHP();
    //    RefreshCatPanel();
    //}

    //private void EnemyTurn()
    //{
    //    int eAtk = enemy.GetDamage();
    //    bool defeated = GameController.Instance.GetSelectedCat().TakeDamage(eAtk, enemy.GetStl());
    //    SetHealthBar(GameController.Instance.GetSelectedCat().GetHpPercent(), true);

    //    if (defeated)
    //    {
    //        SetLossText();
    //        EndBattle();
    //    }
    //}

    //public void NavUp()
    //{
    //    Nav("Up");
    //}

    //public void NavDown()
    //{
    //    Nav("Down");
    //}

    //public void NavLeft()
    //{
    //    Nav("Left");
    //}

    //public void NavRight()
    //{
    //    Nav("Right");
    //}

    ///// <summary>
    ///// 50% Chance of starting a battle.
    ///// </summary>
    ///// <param name="dir"></param>
    //public void Nav(string dir)
    //{
    //    if (inBattle == false)
    //    {
    //        if (Random.Range(0, 2) > 0)
    //        {
    //            NewBattle();
    //        }
    //        else
    //        {
    //            //TODO Nothing?
    //        }
    //    }
    //}

    ///// <summary>
    ///// End a battle
    ///// </summary>
    //public void Retreat()
    //{
    //    //TODO show text on screen
    //    if (inBattle)
    //    {
    //        EndBattle();
    //    }
    //}

}

[System.Serializable]
public class Attack {
    string name = "Scratch";
    int points = 0;
    AttackType type = AttackType.Basic;
    // Basic attack that does damage + points*1
    // TODO change to a case statement 
    public int DamageDealt()
    {
        switch (type)
        {
            case AttackType.Basic:
                return points;
            case AttackType.Double:
                return 2 * points;
            default:
                return points;
        }
    }
}

public enum AttackType
{
    Basic,
    Double
    //DOT
    //Fireup
    //Cooldown
}
