﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Text that starts fading as soon as it is set
/// </summary>
public class FadeOutText : MonoBehaviour
{
    //Fade time in seconds
    public float fadeOutTime;
    public Vector3 floatEndpoint;

    private void Awake()
    {
        SetText("Stripes");
    }

    public void SetText(string t)
    {
        GetComponent<TextMesh>().text = t;
        StartCoroutine(FadeOutRoutine());
    }

    /// <summary>
    /// Change color and float up
    /// </summary>
    /// <returns></returns>
    private IEnumerator FadeOutRoutine()
    {
        TextMesh text = GetComponent<TextMesh>();
        Color originalColor = text.color;


        for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
        {
            text.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t / fadeOutTime));
            Vector3 movingTo = new Vector3(transform.position.x,floatEndpoint.y,transform.position.z);
            transform.position = Vector3.Lerp(transform.position, movingTo, Mathf.Min(1, t / (fadeOutTime*10)));
            yield return null;
        }
        Destroy(gameObject);
    }
}
