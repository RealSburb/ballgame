﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Level
{
    public int levelID;
    public Terrain terrain;
    public Transform[] creatures;
    //TODO public int[] spawnRatio;
    public Material skybox;

    public List<String> GetDroplist()
    {
        List<String> list = new List<string>();

        foreach (Transform t in creatures)
        {
            MonsterBehavior script = t.GetComponent<MonsterBehavior>();
            foreach (String s in script.dropList)
            {
                if (!list.Contains(s))
                {
                    list.Add(s);
                }
            }
        }
        return list;
    }
}
