﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    //public Transform PlayerTransform;
    private Camera cam;
    private Vector3 defaultPos;
    private Vector3 defaultRot;
    private float defaultZoom;

    private float targetZoom;
    private Vector3 targetPos;

    // private Vector3 defaultEuler;
    private Vector3 CAMERA_OFFSET_LEFT = new Vector3(1.94f, 2.46f, -4.13f);
    private Vector3 CAMERA_MIDDLE = new Vector3(0f, 2.46f, -4.13f);
    int SmoothFactor = 6;

    CatInfoPanel infoPanel;
    //TODO get cat data directly?



        // Start is called before the first frame update
        void Start()
    {
        cam = GetComponent<Camera>();
        defaultPos = transform.position;
        defaultRot = transform.eulerAngles;
        defaultZoom = cam.fieldOfView;
        infoPanel = CatInfoPanel.catInfoPanel;
    }

    /// <summary>
    /// Constantly slerping towards a value
    /// </summary>
    void LateUpdate()
    {
        CatData selectedCatData = GameController_Script.control.getSelectedCatData();  
        
        // If a cat selected
        if (selectedCatData != null)
        {
            // Get Transform of selected cat
            Transform cat = GameController_Script.control.getSelectedCat().transform;

            // Offset from Center
            CalculateCameraOffset(cat);

            // Zoom
            if(cam.fieldOfView == defaultZoom)
            {
                ZoomToCatHeight(cat);
            }

            // Slerp towards new position
            transform.position = Vector3.Slerp(transform.position, targetPos, SmoothFactor * Time.deltaTime);
        }
        else // No Cat Selected
        {
            MoveToDefaultCamera();
        }
    }


    private void ZoomToCatHeight(Transform cat)
    {   
        float catSize = cat.gameObject.GetComponent<CatInstance>().getCatData().Size;
        float zoom = (-13*Mathf.Pow(catSize,2)) + (70*catSize) +43;
        cam.fieldOfView = zoom;
        Debug.Log("DefaultZoom " + defaultZoom + " catMod " + catSize + " zoom " + zoom);
        //cam.fieldOfView = Mathf.Lerp(cam.fieldOfView,cam.fieldOfView + catMod, Time.deltaTime * SmoothFactor);
    }

    private void CalculateCameraOffset(Transform cat)
    {
        if (infoPanel.IsMinimized())
        {
            targetPos = cat.position + CAMERA_MIDDLE;
        }
        else
        {
            targetPos = cat.position + CAMERA_OFFSET_LEFT;
        }
    }

    private void MoveToDefaultCamera()
    {
        if (transform.position != defaultPos)
        {
            transform.position = Vector3.Slerp(transform.position, defaultPos, SmoothFactor * Time.deltaTime);
            transform.eulerAngles = defaultRot; // TODO rotate to
            cam.fieldOfView = defaultZoom;
        }
        if (transform.eulerAngles != defaultRot)
        {
            transform.eulerAngles = defaultRot;
        }
    }

}
