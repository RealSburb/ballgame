﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public enum Tab { Cattributes, Genetics };

/// <summary>
/// Displays Info for the selected cat
/// </summary>
public class CatInfoPanel : MonoBehaviour {
    public static CatInfoPanel catInfoPanel;
    //public GameObject panel;
    public GameObject wholePanel;
    public GameObject infoPanel;
    public GameObject minimizeButton;
    public InputField nameInput;
    public Text growText;

    public TabManager tabManager;
    private Tab selectedTab = Tab.Cattributes;
    private bool isMinimized = true;

    //private bool minimized = false;

    /// <summary>
    /// Singleton instance, since there should only ever be one of these panels at a time
    /// </summary>
    void Awake()
    {
        //Debug.Log("InfoPanel Awake");
        // create Singleton
        if (catInfoPanel == null)
        {
            catInfoPanel = this;
            //Debug.Log("InfoPanel set");
        }
        else if (catInfoPanel != this)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Check if the Whole Info Panel is active
    /// </summary>
    /// <returns></returns>
    public bool IsOpen()
    {
        return (wholePanel.activeSelf);
    }

    /// <summary>
    /// Check if the Info Panel is minimized
    /// </summary>
    /// <returns></returns>
    public bool IsMinimized()
    {
        return isMinimized;
    }

    /// <summary>
    /// Load selected Tab
    /// </summary>
    /// <param name="t"></param>
    public void openTab(Tab t)
    {
        // set current tab
        selectedTab = t;
        // Get CatData
        CatData data = GameController_Script.control.getSelectedCatData();
        // Name bar
        nameInput.SetTextWithoutNotify(data.Name);
        // Set Grow Button text
        //int amtToMax = Math.Max(0, 100 - GameController_Script.control.getSelectedCatData().GetPowerups());
        //growText.text = "Grow ("+amtToMax+")";
        // Load inputTab
        tabManager.loadTab(t);
    }

    /// <summary>
    /// Reload whatever is visible on the Cat Info panel.
    /// </summary>
    public void reloadTab()
    {
        //Debug.Log("Reloading" + selectedTab);
        // Get CatData
        CatData data = GameController_Script.control.getSelectedCatData();
        // Name bar
        nameInput.SetTextWithoutNotify(data.Name);
        // Open Current
        openTab(selectedTab);
    }

    ///// <summary>
    ///// Request Grow to Max and then reload CatInfo
    ///// </summary>
    //public void requestPowerup()
    //{
    //    GameController_Script.control.BuyPowerUp();
    //    // Reload Button Text
    //    int amtToMax = Math.Max(0, 100 - GameController_Script.control.getSelectedCatData().GetPowerups());
    //    growText.text = "Grow (amtToMax)";
    //    // Reload Tab
    //    reloadTab();   // display new value
    //}

    /// <summary>
    /// Toggles between the entire panel being open or closed
    /// </summary>
    public void toggle()
    {
        if (wholePanel.activeSelf)
        {
            deactivateInfoPanel();
        }
        else
        {
            ActivateInfoPanel();
        }
    }

    /// <summary>
    /// Deactivate entire panel
    /// </summary>
    public void deactivateInfoPanel()
    {
        wholePanel.SetActive(false);
    }

    /// <summary>
    /// Activate entire panel
    /// </summary>
    public void ActivateInfoPanel()
    {
        wholePanel.SetActive(true);
        reloadTab();   
    }

    /// <summary>
    /// Change selected cat's name
    /// </summary>
    public void changeName()
    {
        CatData data = GameController_Script.control.getSelectedCatData();
        data.Name = nameInput.text;
    }

    /// <summary>
    /// Set selected cat to null
    /// </summary>
    public void DeselectCat()
    {
        // Turn off Interact
        GameController_Script.control.getSelectedCat().GetComponent<CatController>().StartWander();
        // Deselect
        GameController_Script.control.setSelectedCat(null);
        // Close Panel
        deactivateInfoPanel();
    }

    /// <summary>
    /// Toggle hiding the cat info panel without deselecting the cat.
    /// </summary>
    public void MinimizeInfoPanel()
    {
        if (infoPanel.activeSelf)
        {
            infoPanel.SetActive(false);
            minimizeButton.GetComponentInChildren<Text>().text = "<";
            isMinimized = true;
        }
        else
        {
            infoPanel.SetActive(true);
            minimizeButton.GetComponentInChildren<Text>().text = ">";
            isMinimized = false;
        }
        
    }

    /// <summary>
    /// Minimize rather than toggle
    /// </summary>
    public void MinimizeInfo()
    {
        infoPanel.SetActive(false);
        minimizeButton.GetComponentInChildren<Text>().text = "<";
        isMinimized = true;
    }

    public void EnableInteractMode()
    {
        MinimizeInfo();
        GameObject cat = GameController_Script.control.getSelectedCat();
        cat.GetComponent<CatController>().ToggleInteract();
    }
}
